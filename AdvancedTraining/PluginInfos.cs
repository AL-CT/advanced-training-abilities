﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AdvancedTraining
{
    public static class PluginInfos
    {
        public const string PLUGIN_GUID = "uk.daioutzu.plugins.llb.AdvancedTraining";
        public const string PLUGIN_NAME = "AdvancedTraining";
        public const string PLUGIN_VERSION = "2.2.0";
    }
}
