﻿using GameplayEntities;
using LLHandlers;
using UnityEngine;


namespace AdvancedTraining
{
    public class AngleDrawer : MonoBehaviour
    {
        private Material lineMaterial;

        private void Start()
        {
            lineMaterial = new Material(Shader.Find("Hidden/Internal-Colored"));
            lineMaterial.hideFlags = HideFlags.HideAndDontSave;
            lineMaterial.SetInt("_SrcBlend", 5);
            lineMaterial.SetInt("_DstBlend", 10);
            lineMaterial.SetInt("_Cull", 0);
            lineMaterial.SetInt("_ZWrite", 0);
            lineMaterial.SetInt("_ZTest", (int)UnityEngine.Rendering.CompareFunction.Always);
            AudioHandler.PlaySfx(Sfx.MENU_CONFIRM);
        }

        private void OnDestroy()
        {
            if (!(ALDOKEMAOMB.BJDPHEHJJJK(1).NGLDMOLLPLK && ALDOKEMAOMB.BJDPHEHJJJK(1).PNHOIDECPJE))
            {
                hitCancel = true;
                ALDOKEMAOMB.BJDPHEHJJJK(0).JCCIAMJEODH.SetHitstunTimer(ConvertTo.FramesDuration60fps(1));
                BallHandler.instance.GetBall().SetHitstunTimer(ConvertTo.FramesDuration60fps(1));
                counter = 0;
            }
            AudioHandler.PlaySfx(Sfx.MENU_SET);
        }

        float counter = 0;
        float ballCounter = 0;
        private bool hitCancel;
        private bool ballCancel;
        IBGCBLLKIHA[,] pong = new IBGCBLLKIHA[50, 50];
        int[] nAngleArray;
        int[] dAngleArray;
        int[,] neutralAngles;
        int[,] downAirAngles;
        int[,] smashAngles;
        int rollreflect;
        bool roll = false;
        Side preWall;

        HitstunState hitstun;
        Vector3 heading;
        string abilityState;

        public void OnPostRender()
        {
            BallEntity ball = BallHandler.instance.GetBall(0);
            hitstun = ball.ballData.hitstunState;
            PlayerEntity player = ball.GetLastPlayerHitter() != null ? PlayerHandler.instance.GetPlayerEntity(ball.GetLastPlayerHitter().playerIndex) : PlayerHandler.instance.GetPlayerEntity(0);
            Vector3 from = ball.InHitstunByAPlayer() ? ConvertTo.Vector2(ball.GetPosition()) : ConvertTo.Vector2(player.GetCurrentlyActiveHurtbox().bounds.LOLBPNFNKMI);
            heading = (player.playerData.heading == Side.LEFT) ? Vector3.left : Vector3.right;
            abilityState = player.moveableData.abilityState ?? "";
            nAngleArray = new int[] { 0, player.OnGround() ? player.hitAngleDown : player.hitAngleNeutralDownAir, player.hitAngleUp };
            neutralAngles = new int[,] { { 0, 1 }, { player.OnGround() ? player.hitAngleDown : player.hitAngleNeutralDownAir, 4 }, { player.hitAngleUp, 4 } };
            downAirAngles = new int[,] { { 90, 1 }, { player.hitAngleDownAirForward, 4 }, { player.hitAngleDownAirBackward, 4 } };
            dAngleArray = new int[] { 90, player.hitAngleDownAirForward, player.hitAngleDownAirBackward };
            smashAngles = new int[,] { { player.hitAngleSmash, 4 } };

            if (JOMBNFKIHIC.EAENFOJNNGP != OnlineMode.NONE || JOMBNFKIHIC.GDNFJCCCKDM) { Application.Quit(); }

            DrawCube(World.instance.GetStageCenter(), World.instance.stageSize, Color.white, 10f, Alignment.OUTSIDE);
            if (player.IsHittingBall())
            {
                if (player.neutralSwingAbility.IsBeingUsed())
                {
                    for (int i = 0; i < neutralAngles.GetLength(0); i++)
                    {
                        NewAngleDraw(ball, AngleToDirection2(neutralAngles[i, 0], heading), neutralAngles[i, 1]);
                    }
                }
                else if (player.downAirSwingAbility.IsBeingUsed())
                {
                    for (int i = 0; i < downAirAngles.GetLength(0); i++)
                    {
                        NewAngleDraw(ball, AngleToDirection2(downAirAngles[i, 0], heading), downAirAngles[i, 1]);
                    }
                }
                else if (player.smashAbility.IsBeingUsed())
                {
                    for (int i = 0; i < smashAngles.GetLength(0); i++)
                    {
                        NewAngleDraw(ball, AngleToDirection2(smashAngles[i, 0], heading), smashAngles[i, 1]);
                    }
                }

                if (ball.hitableData.hitstunState == HitstunState.DOUBLESTRIKE_STUN)
                {
                    for (int i = 0; i < neutralAngles.GetLength(0); i++)
                    {
                        NewAngleDraw(ball, AngleToDirection2(neutralAngles[i, 0], heading), neutralAngles[i, 1]);
                        if (!isNormalHitstun) NewAngleDraw(ball, AngleToDirection2(neutralAngles[i, 0], -heading), neutralAngles[i, 1]);
                    }
                }

            }

            if (isTraceBall)
            {
                TraceBallOnFollowThrough(ball);
            }

            if (ball.bouncingData.ballState == BallState.CANDYBALL || ball.hitableData.hitstunState == HitstunState.CANDY_STUN)
            {
                NewAngleDraw(ball, ball.bouncingData.flyDirection, 4);
            }

            if ((ALDOKEMAOMB.BJDPHEHJJJK(1).NGLDMOLLPLK && ALDOKEMAOMB.BJDPHEHJJJK(1).PNHOIDECPJE) || isNormalHitstun)
            {
                DrawHit(ref from, player);
            }
            else
            {
                PauseHit(ref from, player);
                if (isPauseBall)
                {
                    BallStun(ref from, player);
                }
            }
            return;
            OptionsOutOfSpecial(player, ball);
            NormalOptions(player, ball);


        }

        IBGCBLLKIHA AngleToDirection2(int angle, Vector2 heading)
        {
            IBGCBLLKIHA dir = Math.AngleToDirection(new HHBCPNCDNDH(angle));
            dir.GCPKPHMKLBN = ConvertTo.Multiply(dir.GCPKPHMKLBN, ConvertTo.Floatf(heading.x));
            return dir;
        }

        void TraceBallOnFollowThrough(BallEntity ball)
        {
            if (preWall != ball.bouncingData.lastWall && ConvertTo.Float(ball.ballData.hitstunTimer) <= 0.3f)
            {
                if (!roll)
                {
                    rollreflect--;
                    roll = true;
                }
                preWall = ball.bouncingData.lastWall;
            }
            else
            {
                roll = false;
            }

            if ((ball.hitableData.hitstunState == HitstunState.NONE && ball.ballData.ballState == BallState.ACTIVE) || ball.InWallTypeStun())
            {
                NewAngleDraw(ball, ball.bouncingData.flyDirection, rollreflect);
            }
            if (ball.InHitstunByAPlayer())
            {
                rollreflect = 4;
            }

        }
        void NewAngleDraw(BallEntity ball, IBGCBLLKIHA angle, int reflections)
        {
            World w = World.instance;
            Vector2 pos = ConvertTo.Vector2(ball.GetPosition());
            Vector2 dir = ConvertTo.Vector2(angle);
            Vector2 side = Vector2.zero;
            bool warpedHorizontal = false;
            bool warpedVertical = false;
            bool candyBall = ball.ballData.hitstunState == HitstunState.CANDY_STUN || ball.ballData.ballState == BallState.CANDYBALL;
            if (ball.ballData.ballState == BallState.CANDYBALL)
            {
                if (ball.bouncingData.specialHitHoriWall == true)
                {
                    warpedHorizontal = true;
                    side = Vector2.left;
                }
                if (ball.bouncingData.specialHitVertWall == true)
                {
                    warpedVertical = true;
                    side = Vector2.down;
                }
            }
            Color color = ConvertTo.Colour(ConvertTo.Float(ConvertTo.DirectionToAngle(ConvertTo.Vector2f(dir))));
            Color candyColor = Color.white;

            if (isShowAim && ConvertTo.Vector2(ball.bouncingData.flyDirection) != dir)
            {
                color.a = 0.2f;
            }

            for (int i = 0; i < reflections; i++)
            {
                if (candyBall && Mathf.Abs(side.x) == 1)
                {
                    warpedHorizontal = true;
                }
                if (candyBall && Mathf.Abs(side.y) == 1)
                {
                    warpedVertical = true;
                }

                Vector2 wBounds = new Vector2(dir.x < 0 ? ConvertTo.Float(w.stageMin.GCPKPHMKLBN) + 0.1f : ConvertTo.Float(w.stageMax.GCPKPHMKLBN) - 0.1f, dir.y < 0 ? ConvertTo.Float(w.stageMin.CGJJEHPPOAN) + 0.1f : ConvertTo.Float(w.stageMax.CGJJEHPPOAN) - 0.1f);
                Vector2 warpBounds = new Vector2(dir.x > 0 ? ConvertTo.Float(w.stageMin.GCPKPHMKLBN) + 0.1f : ConvertTo.Float(w.stageMax.GCPKPHMKLBN) - 0.1f, dir.y > 0 ? ConvertTo.Float(w.stageMin.CGJJEHPPOAN) + 0.1f : ConvertTo.Float(w.stageMax.CGJJEHPPOAN) - 0.1f);
                float x = pos.x;
                float y = pos.y;
                if (dir.x != 0)
                {
                    // Point-Slope Equation
                    float m = dir.y / dir.x;
                    x = (wBounds.y - pos.y + (m * pos.x)) / m;
                    y = m * wBounds.x - m * pos.x + pos.y;
                }

                Vector2 hitSide;
                if (y >= ConvertTo.Float(w.stageMax.CGJJEHPPOAN) || y <= ConvertTo.Float(w.stageMin.CGJJEHPPOAN) || dir.x == 0)
                {
                    hitSide = Vector2.down;
                }
                else
                {
                    hitSide = Vector2.left;
                }

                Debug.Log((Mathf.Abs(hitSide.y) == 1));
                Vector2 to = (Mathf.Abs(hitSide.y) == 1) ? new Vector2(x, wBounds.y) : new Vector2(wBounds.x, y);
                DrawLine(pos, to, candyBall ? candyColor : color, 10);
                pos = to;

                if (Mathf.Abs(hitSide.x) == 1 && warpedHorizontal || Mathf.Abs(hitSide.y) == 1 && warpedVertical)
                {
                    candyBall = false;
                }

                if (Mathf.Abs(hitSide.y) == 1)
                {
                    if (candyBall)
                    {
                        pos.y = warpBounds.y;
                    }
                    else
                    {
                        dir = Vector2.Reflect(dir, Vector2.down);
                        pos.y = wBounds.y;
                    }
                }
                else
                {
                    if (candyBall)
                    {
                        pos.x = warpBounds.x;
                    }
                    else
                    {
                        dir = Vector2.Reflect(dir, Vector2.left);
                        pos.x = wBounds.x;
                    }
                }
                side = hitSide;
            }
        }

        void LineDrawDistance(BallEntity ball, IBGCBLLKIHA angle)
        {

        }

        void NormalOptions(PlayerEntity player, BallEntity ball)
        {
            bool wallSwingUsed = (player.GetCurrentAbility() != null) ? (player.GetCurrentAbility().name == "wallSwing") ? true : false : false;
            Vector3 from = ball.InHitstunByAPlayer() ? ConvertTo.Vector2(ball.GetPosition()) : ConvertTo.Vector2(player.GetCurrentlyActiveHurtbox().bounds.LOLBPNFNKMI);

            bool flag = ball.hitableData.hitstunState == HitstunState.SOUNDWAVE_START_STUN || ball.hitableData.hitstunState == HitstunState.BUBBLE_STUN;
            if (!flag && player.neutralSwingAbility.IsBeingUsed() && !(player.abilityData.abilityState.Contains("THROUGH") || player.abilityData.abilityState.Contains("RETURN")) && !(player.neutralSwingAbility.data.hitSomething && player.abilityData.abilityState.Contains("WOOSH2")))
            {
                if (!player.neutralSwingAbility.data.hitSomething)
                {
                    IBGCBLLKIHA throwOffset = player.throwOffset;
                    throwOffset.GCPKPHMKLBN = ConvertTo.Multiply(throwOffset.GCPKPHMKLBN, HHBCPNCDNDH.NKKIFJJEPOL((decimal)heading.x));
                    IBGCBLLKIHA ibgcbllkiha = ConvertTo.Add(player.GetPosition(), throwOffset);
                    from = ConvertTo.Vector2(ibgcbllkiha);
                }
                for (int i = 0; i <= 2; i++)
                {
                    int reflect = i == 0 ? 2 : 4;
                    DrawReflect(from, nAngleArray[i], heading, reflect);
                }
            }
            // Draws angles for Spike / Down Air Swing
            else if (!flag && player.downAirSwingAbility.IsBeingUsed() && !player.abilityData.abilityState.Contains("OUT") && !player.abilityData.abilityState.Contains("LAND") && !(player.neutralSwingAbility.data.hitSomething && player.abilityData.abilityState.Contains("THROUGH")))
            {
                for (int i = 0; i <= 2; i++)
                {
                    int reflect = i == 0 ? 1 : 4;
                    DrawReflect(from, dAngleArray[i], heading, reflect);
                }
            }
            // Draws angles for Smash
            else if (!flag && player.smashAbility.IsBeingUsed() && !player.abilityData.abilityState.Contains("THROUGH") && !(player.neutralSwingAbility.data.hitSomething && player.abilityData.abilityState.Contains("FRONT_SWING")))
            {
                DrawReflect(from, smashAngles[0, 0], heading, 5);
            }
            // Draws angles for Latch's Wall Swing ability
            else if (wallSwingUsed && !abilityState.Contains("THROUGH") && !(player.neutralSwingAbility.data.hitSomething && player.abilityData.abilityState.Contains("WOOSH2")))
            {
                CrocPlayer croc = FindObjectOfType<CrocPlayer>();
                int angle = (int)ConvertTo.Float(croc.wallClimbDownAim);
                DrawReflect(from, angle, heading, 4);
                DrawReflect(from, nAngleArray[0], heading, 2);
                DrawReflect(from, nAngleArray[2], heading, 4);
            }
            else if (player.pitchAbility.IsBeingUsed() && hitstun == HitstunState.PITCH_STUN)
            {
                IBGCBLLKIHA throwOffset = player.throwOffset;
                throwOffset.GCPKPHMKLBN = ConvertTo.Multiply(throwOffset.GCPKPHMKLBN, HHBCPNCDNDH.NKKIFJJEPOL((decimal)heading.x));
                IBGCBLLKIHA ibgcbllkiha = ConvertTo.Add(player.GetPosition(), throwOffset);
                from = ConvertTo.Vector2(ibgcbllkiha);

                DrawReflect(from, 0, heading, 1);
                DrawReflect(from, 90, heading, 1);
            }
            // Draws angles out of Switch's Kickflip ability while in use (No Ball Hit)
            else if (player.moveableData.playerState == PlayerState.SPECIAL && abilityState.Contains("KICKFLIP"))
            {
                int angle = player.hitAngleDown;
                DrawReflect(from, angle, heading, 3);
                DrawReflect(from, angle, -heading, 3);
            }
        }
        void OptionsOutOfSpecial(PlayerEntity player, BallEntity ball)
        {
            Vector3 vector = (player.entityData.heading == Side.LEFT) ? Vector3.left : Vector3.right;
            Vector3 from = ConvertTo.Vector2(ball.GetPosition());
            string state = player.abilityData.abilityState ?? "";
            //bool hit = player.GetCurrentAbility().data.hitSomething;
            bool neutralSwing = player.neutralSwingAbility.IsBeingUsed();
            bool downAirSwing = player.downAirSwingAbility.IsBeingUsed();
            bool smash = player.smashAbility.IsBeingUsed();
            bool doubleStrikeUsed = (player.GetCurrentAbility() != null) ? (player.GetCurrentAbility().name == "doubleStrike") ? true : false : false;
            Color specialColor = Color.cyan;
            HitstunState hitstun = ball.ballData.hitstunState;

            // Draw the distance Jets' bubble will travel before popping
            if (hitstun == HitstunState.BUBBLE_STUN)
            {
                int speed = 5;
                int time = 45;
                if (neutralSwing)
                {
                    for (int i = 0; i <= 2; i++) DrawDistanceLine(from, ConvertTo.AngleToDirection(nAngleArray[i], vector), time, speed, specialColor);
                    return;
                }
                else if (downAirSwing)
                {
                    for (int i = 0; i <= 2; i++) DrawDistanceLine(from, ConvertTo.AngleToDirection(dAngleArray[i], vector), time, speed, specialColor);
                    return;
                }
                else if (smash)
                {
                    DrawDistanceLine(from, ConvertTo.AngleToDirection(player.hitAngleSmash, vector), time, speed, specialColor);
                    return;
                }
            }
            // Draw the angles out of the start of Sonatas special "Soundwave"
            else if (hitstun == HitstunState.SOUNDWAVE_START_STUN)
            {
                int time = 10; // As frames
                int speed = 32; // As Pixels per frame
                for (int i = -45; i <= 45; i += 45)
                {
                    DrawDistanceLine(from, ConvertTo.AngleToDirection(i, vector), time, speed, Color.red);
                }
            }
            // Draw Dice's angles out of his Cornered special.
            else if (hitstun == HitstunState.PONGWALL_STUN && !(player.playerData.specialAmount >= 1 && ball.ballData.wallGrindSide != Side.UP))
            {

                Vector2 head = new Vector2(ball.bouncingData.pongTurnSide, 0);
                if (ball.ballData.wallGrindSide == Side.UP)
                {
                    Vector2 From2 = from;
                    DrawReflect(From2, 90, Vector3.zero, 1);
                    if (player.playerData.specialAmount < 1)
                    {
                        From2.y = ConvertTo.Float(ConvertTo.Divide(ball.hitbox.bounds.IACOKHPMNGN.GCPKPHMKLBN, HHBCPNCDNDH.NKKIFJJEPOL(2.0m)));
                        DrawReflect(From2, 352, head, 3);
                    }
                }
                DrawReflect(from, ConvertTo.Float(ConvertTo.DirectionToAngle(ball.ballData.flyDirection)), Vector2.right, 4);
            }
            // Draw the angles during Latch's "Eat" ability.
            else if (hitstun == HitstunState.EATEN_STUN && !state.Contains("FOLLOW_THROUGH"))
            {
                IBGCBLLKIHA ballSpitOffset = IBGCBLLKIHA.AJOCFFLIIIH(new IBGCBLLKIHA(25, 15), World.FPIXEL_SIZE);
                ballSpitOffset.GCPKPHMKLBN = ConvertTo.Multiply(ballSpitOffset.GCPKPHMKLBN, HHBCPNCDNDH.NKKIFJJEPOL((decimal)vector.x));
                IBGCBLLKIHA newOffset = IBGCBLLKIHA.GAFCIOAEGKD(player.GetPosition(), ballSpitOffset);

                from = ConvertTo.Vector2(newOffset);
                for (int i = 0; i <= 2; i++)
                {
                    int reflect = i == 0 ? 2 : 4;
                    DrawReflect(from, nAngleArray[i], vector, reflect);
                }
            }
            else if (hitstun == HitstunState.CUFFED_STUN)
            {
                CopPlayer cop = FindObjectOfType<CopPlayer>();
                // Draw the angles out of Nitro's Spike during Special
                if (state.Contains("DOWN"))
                {
                    IBGCBLLKIHA cuffPos = cop.cuffLaunchDownPos;
                    cuffPos.GCPKPHMKLBN = ConvertTo.Multiply(cuffPos.GCPKPHMKLBN, HHBCPNCDNDH.NKKIFJJEPOL((decimal)vector.x));
                    IBGCBLLKIHA newOffset = IBGCBLLKIHA.GAFCIOAEGKD(player.GetPosition(), cuffPos);
                    from = ConvertTo.Vector2(newOffset);

                    for (int i = 0; i <= 2; i++)
                    {
                        DrawReflect(from, dAngleArray[i], vector, 1);
                    }
                }
                // Draw the angles out of Nitro's Swing/Smash during Special
                else
                {
                    IBGCBLLKIHA cuffPos = cop.cuffLaunchPos;
                    cuffPos.GCPKPHMKLBN = ConvertTo.Multiply(cuffPos.GCPKPHMKLBN, HHBCPNCDNDH.NKKIFJJEPOL((decimal)vector.x));
                    IBGCBLLKIHA newOffset = ConvertTo.Add(player.GetPosition(), cuffPos);
                    from = ConvertTo.Vector2(newOffset);
                    // Compare fly dirction to Angle Down and Smash Angle's to determine if cuff storage is being used
                    IBGCBLLKIHA flydir = ball.ballData.flyDirection;
                    if (ConvertTo.Float(flydir.CGJJEHPPOAN) == ConvertTo.Float(Math.AngleToDirection(HHBCPNCDNDH.NKKIFJJEPOL(player.hitAngleDown)).CGJJEHPPOAN))
                    {
                        DrawReflect(from, nAngleArray[1], vector, 1);
                    }
                    else if (ConvertTo.Float(flydir.CGJJEHPPOAN) == ConvertTo.Float(Math.AngleToDirection(HHBCPNCDNDH.NKKIFJJEPOL(player.hitAngleSmash)).CGJJEHPPOAN))
                    {
                        DrawReflect(from, player.hitAngleSmash, vector, 1);
                    }
                    DrawReflect(from, (int)ConvertTo.Float(cop.cuffAngleDown), vector, 1);
                    DrawReflect(from, nAngleArray[0], vector, 1);
                    DrawReflect(from, nAngleArray[2], vector, 1);
                }
            }
            // Draw the angle out of Grid's "Teleport" special
            else if (hitstun == HitstunState.TELEPORT_STUN)
            {
                from = ConvertTo.Vector2(ball.GetPosition());
                if (state.Contains("NEUTRAL"))
                {
                    DrawReflect(from, nAngleArray[0], vector, 2);
                }
                else if (state.Contains("DOWN"))
                {
                    DrawReflect(from, dAngleArray[0], vector, 4);
                }
                else if (state.Contains("SMASH"))
                {
                    DrawReflect(from, player.hitAngleSmash, vector, 4);
                }
            }
            // Draw Angles out of Doombox's Snipe and Soundblast (Crouch Special) abilities
            else if (hitstun == HitstunState.SNIPE_STUN)
            {
                DrawReflect(from, ConvertTo.Float(ConvertTo.DirectionToAngle(ball.ballData.flyDirection)), Vector2.right, 2);
                //DrawReflect(from, FlyDirectionToDirection(ball.ballData.flyDirection), 2, Color.red);
            }
            else if (hitstun == HitstunState.HIT_BY_SOUNDBLAST_STUN)
            {
                DrawReflect(from, 0, vector, 1);
            }
            // Draw Angles out of Switchs' "KickFlip" abilities
            else if (hitstun == HitstunState.KICKFLIP_STUN)
            {
                if (state.Contains("FRONT_HIT") || state.Contains("NEUTRAL_HIT"))
                {
                    int angle = player.hitAngleDown;
                    DrawReflect(from, angle, vector, 5);
                }
                else if (state.Contains("HIT") && state.Contains("KICKFLIP"))
                {
                    int angle = player.hitAngleDown;
                    DrawReflect(from, angle, vector, 5);
                    vector *= -1;
                    DrawReflect(from, angle, vector, 5);
                }
            }
        }
        void PauseHit(ref Vector3 f, PlayerEntity hitter)
        {

            BallEntity ball = BallHandler.instance.GetBall();
            string s = hitter.abilityData.abilityState != null ? hitter.abilityData.abilityState : "";

            if (hitter.IsInHitpause() && ball.InHitstunByAPlayer())
            {
                if (!hitCancel)
                {
                    counter += Time.deltaTime;
                    f = ConvertTo.Vector2(ball.GetPosition());
                    hitter.SetHitstunTimer(new HHBCPNCDNDH(1));
                    ball.SetHitstunTimer(new HHBCPNCDNDH(1));
                }
            }
            // Grid's Teleport ability
            else if (ball.ballData.hitstunState == HitstunState.TELEPORT_STUN)
            {
                if (!hitCancel)
                {
                    if (hitter.GetCurrentAbilityState().name.Contains("START") || hitter.GetCurrentAbilityState().name.Contains("GO"))
                    {
                        counter = 0;
                    }
                    counter += Time.deltaTime;
                    f = ConvertTo.Vector2(ball.GetPosition());
                    hitter.SetHitstunTimer(ConvertTo.FramesDuration60fps(12));
                    ball.SetHitstunTimer(ConvertTo.FramesDuration60fps(12));
                    if (HHBCPNCDNDH.OAHDEOGKOIM(hitter.GetCurrentAbility().data.abilityStateTimer, ConvertTo.FramesDuration60fps(11)))
                    {
                        hitter.GetCurrentAbility().data.abilityStateTimer = ConvertTo.FramesDuration60fps(12);
                    }
                }
            }
            // Doombox Sounblast ability pause
            else if (ball.ballData.hitstunState == HitstunState.HIT_BY_SOUNDBLAST_STUN)
            {
                if (!hitCancel)
                {
                    counter += Time.deltaTime;
                    f = ConvertTo.Vector2(ball.GetPosition());
                    hitter.SetHitstunTimer(new HHBCPNCDNDH(1));
                    ball.SetHitstunTimer(new HHBCPNCDNDH(1));
                }
            }
            // Raptor's Double Strike ability pause
            else if (ball.ballData.hitstunState == HitstunState.DOUBLESTRIKE_STUN)
            {
                if (!hitCancel)
                {
                    counter += Time.deltaTime;
                    f = ConvertTo.Vector2(ball.GetPosition());
                    hitter.SetHitstunTimer(new HHBCPNCDNDH(1));
                    ball.SetHitstunTimer(new HHBCPNCDNDH(1));
                }
                if (InputHandler.GetInput(hitter.player, InputAction.LEFT) || InputHandler.GetInput(hitter.player, InputAction.RIGHT))
                {
                    hitter.TurnHeadingToInput();
                }
            }
            else
            {
                hitCancel = false;
                counter = 0;
            }

            if (counter > 0.48f && InputHandler.GetInput(hitter.player, InputAction.JUMP))
            {
                bool inDS = s.Contains("KID_DOUBLESTRIKE_WIND_UP");
                hitCancel = true;
                hitter.SetHitstunTimer(ConvertTo.FramesDuration60fps(1));
                ball.SetHitstunTimer(ConvertTo.FramesDuration60fps(1));
                counter = 0;
            }
        }
        void DrawHit(ref Vector3 f, PlayerEntity hitter)
        {

            BallEntity ball = BallHandler.instance.GetBall();
            string s = hitter.abilityData.abilityState != null ? hitter.abilityData.abilityState : "";

            if (hitter.IsInHitpause() && ball.InHitstunByAPlayer())
            {

                f = ConvertTo.Vector2(ball.GetPosition());

            }
            else if (hitter.GetCurrentAbility() != null)
            {
                if (hitter.GetCurrentAbility().name == "teleport" && s.Contains("STAY"))
                {

                    f = ConvertTo.Vector2(ball.GetPosition());
                }
                else if (hitter.GetCurrentAbility().name == "soundBlast" && hitter.GetCurrentAbility().data.hitSomething) // Doombox's Special
                {
                    f = ConvertTo.Vector2(ball.GetPosition());
                }
            }
        }
        void BallStun(ref Vector3 f, PlayerEntity hitter)
        {
            BallEntity ball = BallHandler.instance.GetBall();

            if (ball.ballData.hitstunState == HitstunState.PONGWALL_STUN && !(hitter.playerData.specialAmount >= 1 && ball.ballData.wallGrindSide != Side.UP))
            {
                f = ConvertTo.Vector2(ball.GetPosition());
                if (!ballCancel)
                {
                    ballCounter += Time.deltaTime;
                    f = ConvertTo.Vector2(ball.GetPosition());
                    ball.SetHitstunTimer(new HHBCPNCDNDH(1));
                }
            }
            else if (ball.ballData.hitstunState.ToString().Contains("SOUNDWAVE"))
            {
                f = ConvertTo.Vector2(ball.GetPosition());
                if (!ballCancel)
                {
                    ballCounter += Time.deltaTime;
                    f = ConvertTo.Vector2(ball.GetPosition());
                    ball.SetHitstunTimer(new HHBCPNCDNDH(1));
                }

                int speed = 32;
                int time = 10;

                if (ball.ballData.hitstunState == HitstunState.SOUNDWAVE_STUN)
                {

                    for (int i = -135; i <= 180; i += 45)
                    {
                        if (i == -90 || i == 90 || i == -270 || i == 270) continue;
                        DrawDistanceLine(f, ConvertTo.AngleToDirection(i, Vector2.right), time, speed, Color.red);
                    }
                }
                else if (ball.ballData.hitstunState == HitstunState.SOUNDWAVE_CEILING_STUN)
                {
                    for (int i = 0; i <= 180; i += 45)
                    {
                        if (i == -90 || i == 90 || i == -270 || i == 270) continue;
                        DrawDistanceLine(f, ConvertTo.AngleToDirection(i, Vector2.right), time, speed, Color.red);
                    }
                }
                else if (ball.ballData.hitstunState == HitstunState.SOUNDWAVE_FLOOR_STUN)
                {
                    for (int i = -135; i <= 0; i += 45)
                    {
                        if (i == -90 || i == 90 || i == -270 || i == 270) continue;
                        DrawDistanceLine(f, ConvertTo.AngleToDirection(i, Vector2.right), time, speed, Color.red);
                    }
                    DrawDistanceLine(f, ConvertTo.AngleToDirection(180, Vector3.right), time, speed, Color.red);
                }
            }
            else
            {
                ballCancel = false;
                ballCounter = 0;
            }

            if (ballCounter > 0.25f && InputHandler.GetInput(hitter.player, InputAction.JUMP))
            {
                ballCancel = true;
                ball.SetHitstunTimer(ConvertTo.FramesDuration60fps(1));
                ballCounter = 0;
            }
        }
        void DrawReflect(Vector3 from, Vector3 dir, int reflections, Color c)
        {
            if (reflections == 0)
            {
                return;
            }
            lineMaterial.SetPass(0);
            World w = World.instance;
            BallEntity ball = BallHandler.instance.GetBall(0);
            HHBCPNCDNDH halfBall = HHBCPNCDNDH.FCGOICMIBEA(ball.hitbox.bounds.IACOKHPMNGN.GCPKPHMKLBN, HHBCPNCDNDH.NKKIFJJEPOL(2.2m));
            Vector3 old = from;
            Vector3 boundsmin = new Vector3(ConvertTo.Float(HHBCPNCDNDH.GAFCIOAEGKD(w.stageMin.GCPKPHMKLBN, halfBall)), ConvertTo.Float(HHBCPNCDNDH.GAFCIOAEGKD(w.stageMin.CGJJEHPPOAN, halfBall)));
            Vector3 boundsmax = new Vector3(ConvertTo.Float(HHBCPNCDNDH.FCKBPDNEAOG(w.stageMax.GCPKPHMKLBN, halfBall)), ConvertTo.Float(HHBCPNCDNDH.FCKBPDNEAOG(w.stageMax.CGJJEHPPOAN, halfBall)));

            bool reflect = false;
            while (reflect == false)
            {
                if ((from.x > boundsmin.x) && (from.x < boundsmax.x) && (from.y > boundsmin.y) && (from.y < boundsmax.y))
                {
                    from += dir * 0.01f;
                }
                else
                {
                    Vector3 normal = new Vector3();
                    if (from.x < boundsmin.x)
                    {
                        normal = Vector3.left;
                    }
                    else if (from.x > boundsmax.x)
                    {
                        normal = Vector3.right;
                    }
                    else if (from.y < boundsmin.y)
                    {
                        normal = Vector3.up;
                    }
                    else if (from.y > boundsmax.y)
                    {
                        normal = Vector3.down;
                    }

                    reflect = true;
                    from -= dir * 0.01f;
                    dir = Vector3.Reflect(dir, normal);
                }
            }
            DrawLine(old, from, c, 10);
            DrawReflect(from, dir, reflections - 1, c);
        }

        /// <summary>
        /// Draws line reflections with the colour based on the angle
        /// </summary>
        /// <param name="from">Location to start the first line</param>
        /// <param name="angle"></param>
        /// <param name="vector"></param>
        /// <param name="reflections"></param>
        void DrawReflect(Vector3 from, float angle, Vector3 vector, int reflections)
        {
            Vector3 dir = ConvertTo.AngleToDirection(angle, vector);  // converts the angle to a vector
            Color c = ConvertTo.Colour(angle); // Set's the colour bsaed on the angle being processed


            if (reflections == 0)
            {
                return;
            }
            lineMaterial.SetPass(0);
            World w = World.instance;
            BallEntity ball = BallHandler.instance.GetBall(0);
            HHBCPNCDNDH halfBall = HHBCPNCDNDH.FCGOICMIBEA(ball.hitbox.bounds.IACOKHPMNGN.GCPKPHMKLBN, HHBCPNCDNDH.NKKIFJJEPOL(2.2m));
            Vector3 old = from;
            Vector3 boundsmin = new Vector3(ConvertTo.Float(HHBCPNCDNDH.GAFCIOAEGKD(w.stageMin.GCPKPHMKLBN, halfBall)), ConvertTo.Float(HHBCPNCDNDH.GAFCIOAEGKD(w.stageMin.CGJJEHPPOAN, halfBall)));
            Vector3 boundsmax = new Vector3(ConvertTo.Float(HHBCPNCDNDH.FCKBPDNEAOG(w.stageMax.GCPKPHMKLBN, halfBall)), ConvertTo.Float(HHBCPNCDNDH.FCKBPDNEAOG(w.stageMax.CGJJEHPPOAN, halfBall)));

            bool reflect = false;
            while (reflect == false)
            {
                if ((from.x > boundsmin.x) && (from.x < boundsmax.x) && (from.y > boundsmin.y) && (from.y < boundsmax.y))
                {
                    from += dir * 0.01f;
                }
                else
                {
                    Vector3 normal = new Vector3();
                    if (from.x < boundsmin.x)
                    {
                        normal = Vector3.left;
                    }
                    else if (from.x > boundsmax.x)
                    {
                        normal = Vector3.right;
                    }
                    else if (from.y < boundsmin.y)
                    {
                        normal = Vector3.up;
                    }
                    else if (from.y > boundsmax.y)
                    {
                        normal = Vector3.down;
                    }

                    reflect = true;
                    from -= dir * 0.01f;
                    dir = Vector3.Reflect(dir, normal);
                }
            }
            DrawLine(old, from, c, 10);
            DrawReflect(from, dir, reflections - 1, c);
        }

        void DrawCandyChain(Vector3 from, Vector3 dir, Vector3 side, int reflections, Color c, bool warpX = false, bool warpY = false)
        {
            if (reflections == 0)
            {
                return;
            }
            if (side == Vector3.left || side == Vector3.right)
            {
                warpX = true;
            }
            if (side == Vector3.up || side == Vector3.down)
            {
                warpY = true;
            }

            World w = World.instance;
            Vector3 old = from;
            Vector3 next = from;
            HHBCPNCDNDH halfBall = ConvertTo.Divide(BallHandler.instance.GetBall().hitbox.bounds.IACOKHPMNGN.GCPKPHMKLBN, HHBCPNCDNDH.NKKIFJJEPOL(2.0m));
            Vector3 boundsmin = ConvertTo.Vector2(ConvertTo.Add(w.stageMin.GCPKPHMKLBN, halfBall), ConvertTo.Add(w.stageMin.CGJJEHPPOAN, halfBall));
            Vector3 boundsmax = ConvertTo.Vector2(ConvertTo.Subtract(w.stageMax.GCPKPHMKLBN, halfBall), ConvertTo.Subtract(w.stageMax.CGJJEHPPOAN, halfBall));
            Vector3 normal = new Vector3();

            bool wallTouch = false;
            while (wallTouch == false)
            {
                if ((from.x > boundsmin.x) && (from.x < boundsmax.x) && (from.y > boundsmin.y) && (from.y < boundsmax.y))
                {
                    from += dir * 0.01f;
                }
                else
                {
                    if (from.x < boundsmin.x)
                    {
                        normal = Vector3.left;
                    }
                    else if (from.x > boundsmax.x)
                    {
                        normal = Vector3.right;
                    }
                    else if (from.y < boundsmin.y)
                    {
                        normal = Vector3.down;
                    }
                    else if (from.y > boundsmax.y)
                    {
                        normal = Vector3.up;
                    }

                    wallTouch = true;
                    from -= dir * 0.01f;
                    next = from;

                    if ((normal == Vector3.left || normal == Vector3.right) && warpX || (normal == Vector3.up || normal == Vector3.down) && warpY)
                    {
                        dir = Vector3.Reflect(dir, normal);
                    }
                    else
                    {
                        if (normal == Vector3.left)
                        {
                            next.x = boundsmax.x;
                            next += dir * 0.01f;
                        }

                        if (normal == Vector3.right)
                        {
                            next.x = boundsmin.x;
                            next += dir * 0.01f;
                        }

                        if (normal == Vector3.up)
                        {
                            next.y = boundsmin.y;
                            next += dir * 0.01f;
                        }

                        if (normal == Vector3.down)
                        {
                            next.y = boundsmax.y;
                            next += dir * 0.01f;
                        }
                    }
                }
            }
            DrawLine(old, from, c, 10);
            if (side == normal)
            {
                DrawReflect(from, dir, reflections - 1, c);
            }
            else
            {
                DrawCandyChain(next, dir, normal, reflections - 1, c, warpX, warpY);
            }
        }
        /// <summary>
        /// Like DrawReflect, but it's to show Candy Chains
        /// </summary>
        /// <param name="from"></param>
        /// <param name="angle"></param>
        /// <param name="vector"></param>
        /// <param name="reflections"></param>
        /// <param name="wall">Must be set as Vector3.zero</param>
        /// <param name="warpX">Leave as Default</param>
        /// <param name="warpY">Leave as Default</param>
        void DrawCandyChain(Vector3 from, float angle, Vector2 vector, int reflections, Vector3 wall, bool warpX = false, bool warpY = false)
        {
            Color c = ConvertTo.Colour(angle); // Set's the colour bsaed on the angle being processed
            Vector3 dir = ConvertTo.AngleToDirection(angle, vector);

            if (reflections == 0)
            {
                return;
            }
            if (wall == Vector3.left || wall == Vector3.right)
            {
                warpX = true;
            }
            if (wall == Vector3.up || wall == Vector3.down)
            {
                warpY = true;
            }

            World w = World.instance;
            Vector3 old = from;
            Vector3 next = from;
            BallEntity ball = BallHandler.instance.GetBall();
            HHBCPNCDNDH halfBall = ConvertTo.Divide(ball.hitbox.bounds.IACOKHPMNGN.GCPKPHMKLBN, HHBCPNCDNDH.NKKIFJJEPOL(2.0m));
            Vector3 boundsmin = ConvertTo.Vector2(ConvertTo.Add(w.stageMin.GCPKPHMKLBN, halfBall), ConvertTo.Add(w.stageMin.CGJJEHPPOAN, halfBall));
            Vector3 boundsmax = ConvertTo.Vector2(ConvertTo.Subtract(w.stageMax.GCPKPHMKLBN, halfBall), ConvertTo.Subtract(w.stageMax.CGJJEHPPOAN, halfBall));
            Vector3 normal = new Vector3();

            IBGCBLLKIHA flydir = ball.ballData.flyDirection;
            if (ConvertTo.Float(flydir.CGJJEHPPOAN) != dir.y)
            {
                c.a = 0.25f;
            }

            bool wallTouch = false;
            while (wallTouch == false)
            {
                if ((from.x > boundsmin.x) && (from.x < boundsmax.x) && (from.y > boundsmin.y) && (from.y < boundsmax.y))
                {
                    from += dir * 0.01f;
                }
                else
                {
                    if (from.x < boundsmin.x)
                    {
                        normal = Vector3.left;
                    }
                    else if (from.x > boundsmax.x)
                    {
                        normal = Vector3.right;
                    }
                    else if (from.y < boundsmin.y)
                    {
                        normal = Vector3.down;
                    }
                    else if (from.y > boundsmax.y)
                    {
                        normal = Vector3.up;
                    }

                    wallTouch = true;
                    from -= dir * 0.01f;
                    next = from;

                    if ((normal == Vector3.left || normal == Vector3.right) && warpX || (normal == Vector3.up || normal == Vector3.down) && warpY)
                    {
                        dir = Vector3.Reflect(dir, normal);
                    }
                    else
                    {
                        if (normal == Vector3.left)
                        {
                            next.x = boundsmax.x;
                            next += dir * 0.01f;
                        }

                        if (normal == Vector3.right)
                        {
                            next.x = boundsmin.x;
                            next += dir * 0.01f;
                        }

                        if (normal == Vector3.up)
                        {
                            next.y = boundsmin.y;
                            next += dir * 0.01f;
                        }

                        if (normal == Vector3.down)
                        {
                            next.y = boundsmax.y;
                            next += dir * 0.01f;
                        }
                    }
                }
            }
            DrawLine(old, from, c, 10);
            if (wall == normal)
            {
                DrawReflect(from, dir, reflections - 1, c);
            }
            else
            {
                DrawCandyChain(next, dir, normal, reflections - 1, c, warpX, warpY);
            }
        }

        /// <summary>
        /// Draws a line from a single point with a duration and speed determining how far to draw the line
        /// </summary>
        /// <param name="from"></param>
        /// <param name="dir"></param>
        /// <param name="durationF"></param>
        /// <param name="speedF"></param>
        /// <param name="color"></param>
        void DrawDistanceLine(Vector3 from, Vector3 dir, int durationF, int speedF, Color color)
        {
            BallEntity ball = BallHandler.instance.GetBall(0);
            float distance = (speedF * durationF) * 0.01f;
            HHBCPNCDNDH halfBall = ConvertTo.Divide(ball.hitbox.bounds.IACOKHPMNGN.GCPKPHMKLBN, ConvertTo.Floatf(2.2f));
            Vector3 boundsmin = ConvertTo.Vector2(ConvertTo.Add(World.instance.stageMin.GCPKPHMKLBN, halfBall), ConvertTo.Add(World.instance.stageMin.CGJJEHPPOAN, halfBall));
            Vector3 boundsmax = ConvertTo.Vector2(ConvertTo.Subtract(World.instance.stageMax.GCPKPHMKLBN, halfBall), ConvertTo.Subtract(World.instance.stageMax.CGJJEHPPOAN, halfBall));

            Vector3 to = from;
            to += dir * distance;
            // Prevents the line being drawn out of bounds
            while ((to.x < boundsmin.x) || (to.x > boundsmax.x) || (to.y < boundsmin.y) || (to.y > boundsmax.y))
            {
                to -= dir * 0.01f;
            }
            DrawLine(from, to, color, 10);
        }
        /// <summary>
        /// Draws a line from A to B, with a colour and line thickness
        /// </summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <param name="color"></param>
        /// <param name="thickness">How wide the line is in pixels</param>
        void DrawLine(Vector2 start, Vector2 end, Color color, int thickness)
        {
            float width = thickness * 0.01f; // converts thickness to represent pixels
            BallEntity ball = BallHandler.instance.GetBall(0);
            lineMaterial.SetPass(0);
            Camera camera = GameCamera.gameplayCam;
            GL.PushMatrix();
            GL.Begin(GL.QUADS);
            GL.LoadProjectionMatrix(camera.projectionMatrix);

            float dx = start.x - end.x;
            float dy = start.y - end.y;

            Vector2 rightSide = new Vector2(dy, -dx);
            if (rightSide.magnitude > 0)
            {
                rightSide.Normalize();
                rightSide.Set(rightSide.x * width / 2, rightSide.y * width / 2);
            }
            Vector2 leftSide = new Vector2(-dy, dx);
            if (leftSide.magnitude > 0)
            {
                leftSide.Normalize();
                leftSide.Set(leftSide.x * width / 2, leftSide.y * width / 2);
            }

            Vector2 one = leftSide + start;
            Vector2 two = rightSide + start;
            Vector2 three = rightSide + end;
            Vector2 four = leftSide + end;
            Color color2 = color * 0.25f;
            color2.a = color.a;
            GL.Color(color2);
            GL.Vertex3(one.x, one.y, 0);
            GL.Vertex3(two.x, two.y, 0);
            GL.Color(color);
            GL.Vertex3(three.x, three.y, 0);
            GL.Vertex3(four.x, four.y, 0);
            GL.End();
            GL.PopMatrix();

            color *= 0.45f;
            color.a = color2.a;
            DrawCube(new IBGCBLLKIHA(HHBCPNCDNDH.NKKIFJJEPOL((decimal)start.x), HHBCPNCDNDH.NKKIFJJEPOL((decimal)start.y)), ball.hitbox.bounds.IACOKHPMNGN, color, 10f);
            DrawCube(new IBGCBLLKIHA(HHBCPNCDNDH.NKKIFJJEPOL((decimal)end.x), HHBCPNCDNDH.NKKIFJJEPOL((decimal)end.y)), ball.hitbox.bounds.IACOKHPMNGN, color, 10f);
        }

        void DrawCube(IBGCBLLKIHA center, IBGCBLLKIHA size, Color color, float thicc = 4f, Alignment align = Alignment.INSIDE)
        {
            lineMaterial.SetPass(0);
            thicc *= 0.01f;
            Camera camera = GameCamera.gameplayCam;
            GL.PushMatrix();
            GL.Begin(7);
            GL.Color(color);
            GL.LoadProjectionMatrix(camera.projectionMatrix);
            Vector3 vector = ConvertTo.Vector2(ConvertTo.Subtract(center.GCPKPHMKLBN, ConvertTo.Multiply(size.GCPKPHMKLBN, HHBCPNCDNDH.GMEDDLALMGA)), ConvertTo.Subtract(center.CGJJEHPPOAN, ConvertTo.Multiply(size.CGJJEHPPOAN, HHBCPNCDNDH.GMEDDLALMGA)));// Bottom Left
            Vector3 vector2 = ConvertTo.Vector2(ConvertTo.Subtract(center.GCPKPHMKLBN, ConvertTo.Multiply(size.GCPKPHMKLBN, HHBCPNCDNDH.GMEDDLALMGA)), ConvertTo.Add(center.CGJJEHPPOAN, ConvertTo.Multiply(size.CGJJEHPPOAN, HHBCPNCDNDH.GMEDDLALMGA)));// Top Left
            Vector3 vector3 = ConvertTo.Vector2(ConvertTo.Add(center.GCPKPHMKLBN, ConvertTo.Multiply(size.GCPKPHMKLBN, HHBCPNCDNDH.GMEDDLALMGA)), ConvertTo.Add(center.CGJJEHPPOAN, ConvertTo.Multiply(size.CGJJEHPPOAN, HHBCPNCDNDH.GMEDDLALMGA)));// Top Right
            Vector3 vector4 = ConvertTo.Vector2(ConvertTo.Add(center.GCPKPHMKLBN, ConvertTo.Multiply(size.GCPKPHMKLBN, HHBCPNCDNDH.GMEDDLALMGA)), ConvertTo.Subtract(center.CGJJEHPPOAN, ConvertTo.Multiply(size.CGJJEHPPOAN, HHBCPNCDNDH.GMEDDLALMGA)));// Bottom Righ
            Vector3 offsetX = new Vector3(thicc, 0, 0);
            Vector3 offsetY = new Vector3(0, thicc * (byte)align, 0);
            Vector3 offsetXY = new Vector3(thicc, thicc, 0);
            Vector3 offsetXY2 = new Vector3(thicc, -thicc, 0);

            #region Left Side
            GL.Vertex(vector - offsetY);
            GL.Vertex(vector2 + offsetY);
            if (align == Alignment.INSIDE)
            {
                GL.Vertex(vector2 + offsetX);
                GL.Vertex(vector + offsetX);
            }
            else
            {
                GL.Vertex(vector2 - offsetXY2);
                GL.Vertex(vector - offsetXY);
            }
            #endregion
            #region  Top Side
            GL.Vertex(vector2);
            GL.Vertex(vector3);
            if (align == Alignment.INSIDE)
            {
                GL.Vertex3(vector3.x, vector3.y - thicc, vector3.z);
                GL.Vertex3(vector2.x, vector2.y - thicc, vector2.z);
            }
            else
            {
                GL.Vertex3(vector3.x, vector3.y + thicc, vector3.z);
                GL.Vertex3(vector2.x, vector2.y + thicc, vector2.z);
            }
            #endregion
            #region Right Side
            GL.Vertex(vector3 + offsetY);
            GL.Vertex(vector4 - offsetY);
            if (align == Alignment.INSIDE)
            {
                GL.Vertex(vector4 - offsetX);
                GL.Vertex(vector3 - offsetX);
            }
            else
            {
                GL.Vertex(vector4 + offsetXY2);
                GL.Vertex(vector3 + offsetXY);
            }
            #endregion
            #region Bottom Side
            GL.Vertex(vector4);
            GL.Vertex(vector);
            if (align == Alignment.INSIDE)
            {
                GL.Vertex3(vector.x, vector.y + thicc, vector.z);
                GL.Vertex3(vector4.x, vector4.y + thicc, vector4.z);
            }
            else
            {
                GL.Vertex3(vector.x, vector.y - thicc, vector.z);
                GL.Vertex3(vector4.x, vector4.y - thicc, vector4.z);
            }
            #endregion

            GL.End();
            GL.PopMatrix();
        }

        void DrawCircle(IBGCBLLKIHA center, HHBCPNCDNDH radius, Color color)
        {
            Camera camera = GameCamera.gameplayCam;
            float num = ConvertTo.Float(radius);
            float num2 = (num - 0.1f <= 0f) ? 0f : (num - 0.1f);
            GL.PushMatrix();
            GL.Begin(7);
            GL.Color(color);
            GL.LoadProjectionMatrix(camera.projectionMatrix);
            for (float num3 = 0f; num3 < 360f; num3 += 1f)
            {
                GL.Vertex3(ConvertTo.Float(center.GCPKPHMKLBN) + Mathf.Cos(num3 * 0.0174532924f) * num, ConvertTo.Float(center.CGJJEHPPOAN) + Mathf.Sin(num3 * 0.0174532924f) * num, 0f);
                GL.Vertex3(ConvertTo.Float(center.GCPKPHMKLBN) + Mathf.Cos((num3 + 1f) * 0.0174532924f) * num, ConvertTo.Float(center.CGJJEHPPOAN) + Mathf.Sin((num3 + 1f) * 0.0174532924f) * num, 0f);
                GL.Vertex3(ConvertTo.Float(center.GCPKPHMKLBN) + Mathf.Cos(num3 * 0.0174532924f) * num2, ConvertTo.Float(center.CGJJEHPPOAN) + Mathf.Sin(num3 * 0.0174532924f) * num2, 0f);
                GL.Vertex3(ConvertTo.Float(center.GCPKPHMKLBN) + Mathf.Cos((num3 + 1f) * 0.0174532924f) * num2, ConvertTo.Float(center.CGJJEHPPOAN) + Mathf.Sin((num3 + 1f) * 0.0174532924f) * num2, 0f);
            }
            GL.End();
            GL.PopMatrix();
        }

        Rect _angleDrawerRect = new Rect(10, Screen.height - 250, 240, 240);
        private bool isNormalHitstun;
        private bool isShowAim;
        private bool isTraceBall;
        private bool isPauseBall;

        void OnGUI()
        {
            _angleDrawerRect = GUI.Window(10, _angleDrawerRect, AngleDrawerWindow, "Angle Drawer", ATStyle.windStyle);
        }
        void AngleDrawerWindow(int wId)
        {
            GUIStyle labStyle = new GUIStyle(GUI.skin.label)
            {
                fontSize = 16,
                alignment = TextAnchor.MiddleCenter,
                stretchHeight = true,
            };
            Rect window = _angleDrawerRect;
            GUILayoutOption minHeight = GUILayout.MinHeight(40);
            GUILayout.BeginArea(new Rect(0, 0, window.width, window.height), ATStyle.border);
            GUILayout.BeginVertical(ATStyle.mainStyle);
            GUILayout.BeginHorizontal(minHeight);
            GUILayout.Label("Normal Hitstun", labStyle);
            isNormalHitstun = GUILayout.Toggle(isNormalHitstun, isNormalHitstun.ToString(), ATStyle.partBtn);
            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal(minHeight);
            GUILayout.Label("Show Aim Choice", labStyle);
            isShowAim = GUILayout.Toggle(isShowAim, isShowAim.ToString(), ATStyle.partBtn);
            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal(minHeight);
            GUILayout.Label("Trace ball follow through", labStyle);
            isTraceBall = GUILayout.Toggle(isTraceBall, isTraceBall.ToString(), ATStyle.partBtn);
            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal(minHeight);
            GUILayout.Label("Pause Special Balls", labStyle);
            isPauseBall = GUILayout.Toggle(isPauseBall, isPauseBall.ToString(), ATStyle.partBtn);
            GUILayout.EndHorizontal();
            GUILayout.FlexibleSpace();
            GUILayout.EndVertical();
            GUILayout.EndArea();
            GUI.DragWindow(new Rect(0, 0, window.width, window.height));
        }
        public enum Alignment : byte
        {
            INSIDE,
            OUTSIDE,
        }
    }

}
