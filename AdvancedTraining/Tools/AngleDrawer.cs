﻿using GameplayEntities;
using LLHandlers;
using UnityEngine;
using BepInEx.Logging;
using LLBML.Math;
using LLBML.Graphic;


namespace AdvancedTraining
{
    public class AngleDrawer : MonoBehaviour
    {
        private static ManualLogSource Logger => AdvancedTraining.Log;
        private static Material lineMaterial;

        private void Start()
        {
            lineMaterial = new Material(Shader.Find("Hidden/Internal-Colored"))
            {
                hideFlags = HideFlags.HideAndDontSave
            };
            lineMaterial.SetInt("_SrcBlend", 5);
            lineMaterial.SetInt("_DstBlend", 10);
            lineMaterial.SetInt("_Cull", 0);
            lineMaterial.SetInt("_ZWrite", 0);
            lineMaterial.SetInt("_ZTest", (int)UnityEngine.Rendering.CompareFunction.Always);
            AudioHandler.PlaySfx(Sfx.MENU_CONFIRM);
            if (!(ALDOKEMAOMB.BJDPHEHJJJK(1).NGLDMOLLPLK && ALDOKEMAOMB.BJDPHEHJJJK(1).PNHOIDECPJE))
            {
                isPausedHitstun = true;
                isPauseBall = true;
            }
            if (AdvancedTraining.IsOnline) { Application.Quit(); }
        }

        private void OnDestroy()
        {
            if (!(ALDOKEMAOMB.BJDPHEHJJJK(1).NGLDMOLLPLK && ALDOKEMAOMB.BJDPHEHJJJK(1).PNHOIDECPJE))
            {
                hitCancel = true;
                ballCancel = true;
                ALDOKEMAOMB.BJDPHEHJJJK(0).JCCIAMJEODH.SetHitstunTimer(ConvertTo.FramesDuration60fps(1));
                BallHandler.instance.GetBall().SetHitstunTimer(ConvertTo.FramesDuration60fps(1));
                hitCounter = 0;
                ballCounter = 0;
            }
            AudioHandler.PlaySfx(Sfx.MENU_SET);
        }

        float hitCounter = 0;
        float ballCounter = 0;
        private bool hitCancel;
        private bool ballCancel;
        readonly IBGCBLLKIHA[,] pong = new IBGCBLLKIHA[50, 50];
        int[,] neutralAngles;
        int[,] downAirAngles;
        int[,] smashAngles;
        bool trace = false;
        int traceReflect;
        int traceReflectAmount = 0;
        int downReflections = 4;
        int upReflections = 4;
        int spikefReflections = 4;
        int spikebReflections = 4;
        int smashReflections = 4;
        Side preWall;

        HitstunState hitstun;
        Vector3 heading;

        public void OnPostRender()
        {
            if (AdvancedTraining.IsOnline && AdvancedTraining.CurrentGameMode != GameMode.TRAINING)
            {
                Logger.LogError("[LLBMM] Terminating \"AngleDrawer\" detected online");
                AudioHandler.PlaySfx(Sfx.KILL);
                Application.Quit();
                return;
            }
            Draw.Cube(World.instance.GetStageCenter(), World.instance.stageSize, Color.white, 10f, Draw.Alignment.OUTSIDE);
            BallEntity ball = BallHandler.instance.GetBall(0);
            PlayerEntity player = ball.GetLastPlayerHitter() == null ? PlayerHandler.instance.GetPlayerEntity(0) : PlayerHandler.instance.GetPlayerEntity(ball.GetLastPlayerHitter().playerIndex);

            hitstun = ball.ballData.hitstunState;
            heading = (player.playerData.heading == Side.LEFT) ? Vector3.left : Vector3.right;
            string state = player.abilityData.abilityState ?? "";
            bool wallSwingUsed = (player.GetCurrentAbility() != null) && (player.GetCurrentAbility().name == "wallSwing");
            neutralAngles = new int[,] { { 0, 1 }, { player.OnGround() ? player.hitAngleDown : player.hitAngleNeutralDownAir, downReflections }, { player.hitAngleUp, upReflections } };
            downAirAngles = new int[,] { { 90, 1 }, { player.hitAngleDownAirForward, spikefReflections }, { player.hitAngleDownAirBackward, spikebReflections } };
            smashAngles = new int[,] { { player.hitAngleSmash, smashReflections } };


            if (JOMBNFKIHIC.EAENFOJNNGP != OnlineMode.NONE || JOMBNFKIHIC.GDNFJCCCKDM) { Application.Quit(); }

            if (InHitstunByAPlayer(ball) && ball.hitableData.hitstunState != HitstunState.BUNT_STUN && ball.hitableData.hitstunState != HitstunState.EATEN_STUN)
            {
                Vector3 from = (Vector2)(ball.InHitstunByAPlayer() ? (Vector2f)ball.GetPosition() : (Vector2f)player.GetCurrentlyActiveHurtbox().bounds.LOLBPNFNKMI);

                if (hitstun == HitstunState.SOUNDWAVE_START_STUN)
                {
                    int time = 10; // As frames
                    int speed = 32; // As Pixels per frame
                    for (int i = -45; i <= 45; i += 45)
                    {
                        DrawDistanceLine(from, ConvertTo.AngleToDirection(i, heading), time, speed, ConvertTo.Colour(i));
                    }
                }
                else if (hitstun == HitstunState.BUBBLE_STUN)
                {
                    int speed = 5;
                    int time = 45;
                    if (player.neutralSwingAbility.IsBeingUsed())
                    {
                        for (int i = 0; i <= 2; i++) DrawDistanceLine(from, ConvertTo.AngleToDirection(neutralAngles[i, 0], heading), time, speed, Color.cyan);
                    }
                    else if (player.downAirSwingAbility.IsBeingUsed())
                    {
                        for (int i = 0; i <= 2; i++) DrawDistanceLine(from, ConvertTo.AngleToDirection(downAirAngles[i, 0], heading), time, speed, Color.cyan);
                    }
                    else if (player.smashAbility.IsBeingUsed())
                    {
                        DrawDistanceLine(from, ConvertTo.AngleToDirection(player.hitAngleSmash, heading), time, speed, Color.cyan);
                    }
                }
                else if (player.neutralSwingAbility.IsBeingUsed())
                {
                    for (int i = 0; i < neutralAngles.GetLength(0); i++)
                    {
                        NewAngleDraw(ball, AngleToDirection(neutralAngles[i, 0], heading), neutralAngles[i, 1]);
                    }
                }
                else if (player.downAirSwingAbility.IsBeingUsed())
                {
                    for (int i = 0; i < downAirAngles.GetLength(0); i++)
                    {
                        NewAngleDraw(ball, AngleToDirection(downAirAngles[i, 0], heading), downAirAngles[i, 1]);
                    }
                }
                else if (player.smashAbility.IsBeingUsed())
                {
                    for (int i = 0; i < smashAngles.GetLength(0); i++)
                    {
                        NewAngleDraw(ball, AngleToDirection(smashAngles[i, 0], heading), smashAngles[i, 1]);
                    }
                }
                else if (hitstun == HitstunState.DOUBLESTRIKE_STUN)
                {
                    for (int i = 0; i < neutralAngles.GetLength(0); i++)
                    {
                        NewAngleDraw(ball, AngleToDirection(neutralAngles[i, 0], heading), neutralAngles[i, 1]);
                        if (isPausedHitstun) NewAngleDraw(ball, AngleToDirection(neutralAngles[i, 0], -heading), neutralAngles[i, 1]);
                    }
                    if (InputHandler.GetInput(player.player, InputAction.LEFT) || InputHandler.GetInput(player.player, InputAction.RIGHT))
                    {
                        player.TurnHeadingToInput();
                    }
                }
                else if (hitstun == HitstunState.TELEPORT_STUN)
                {
                    if (state.Contains("NEUTRAL") || state.Contains("DOWN"))
                    {
                        NewAngleDraw(ball, ball.bouncingData.flyDirection, 1);
                    }
                    else if (state.Contains("SMASH"))
                    {
                        NewAngleDraw(ball, AngleToDirection(smashAngles[0, 0], heading), smashAngles[0, 1]);
                    }
                }
                else if (hitstun == HitstunState.CUFFED_STUN)
                {
                    CopPlayer cop = (CopPlayer)player;
                    IBGCBLLKIHA cuffPos = state.Contains("DOWN") ? cop.cuffLaunchDownPos : cop.cuffLaunchPos;
                    cuffPos.GCPKPHMKLBN = ConvertTo.Multiply(cuffPos.GCPKPHMKLBN, HHBCPNCDNDH.NKKIFJJEPOL((int)heading.x));
                    IBGCBLLKIHA newOffset = ConvertTo.Add(player.GetPosition(), cuffPos);
                    ball.SetPosition(newOffset);
                    if (state.Contains("DOWN"))
                    {
                        for (int i = 0; i <= 2; i++)
                        {
                            NewAngleDraw(ball, AngleToDirection(downAirAngles[i, 0], heading), 1);
                        }
                    }
                    else
                    {
                        IBGCBLLKIHA flydir = ball.ballData.flyDirection;
                        // Compare fly dirction to Angle Down and Smash Angle's to determine if cuff storage is being used
                        if (ConvertTo.Float(flydir.CGJJEHPPOAN) == ConvertTo.Float(Math.AngleToDirection(HHBCPNCDNDH.NKKIFJJEPOL(player.hitAngleDown)).CGJJEHPPOAN) || ConvertTo.Float(flydir.CGJJEHPPOAN) == ConvertTo.Float(Math.AngleToDirection(HHBCPNCDNDH.NKKIFJJEPOL(player.hitAngleSmash)).CGJJEHPPOAN))
                        {
                            NewAngleDraw(ball, ball.bouncingData.flyDirection, 1);
                        }
                        NewAngleDraw(ball, AngleToDirection(neutralAngles[0, 0], heading), 1);
                        NewAngleDraw(ball, AngleToDirection((int)ConvertTo.Float(cop.cuffAngleDown), heading), 1);
                        NewAngleDraw(ball, AngleToDirection(neutralAngles[2, 0], heading), 1);
                    }
                }
                else if (hitstun == HitstunState.KICKFLIP_STUN)
                {
                    int angle = player.hitAngleDown;
                    NewAngleDraw(ball, AngleToDirection(angle, heading), neutralAngles[1, 1]);
                    if (!state.Contains("FRONT") && !state.Contains("NEUTRAL"))
                    {
                        NewAngleDraw(ball, AngleToDirection(angle, -heading), neutralAngles[1, 1]);
                    }
                }
                else if (hitstun == HitstunState.HIT_BY_SOUNDBLAST_STUN)
                {
                    NewAngleDraw(ball, AngleToDirection(neutralAngles[0, 0], heading), 2);
                }
                else if (hitstun == HitstunState.SNIPE_STUN)
                {
                    NewAngleDraw(ball, ball.bouncingData.flyDirection, 2);
                }
                else if (wallSwingUsed)
                {
                    CrocPlayer croc = (CrocPlayer)player;
                    int angle = (int)ConvertTo.Float(croc.wallClimbDownAim);
                    NewAngleDraw(ball, AngleToDirection(angle, heading), neutralAngles[1, 1]);
                    NewAngleDraw(ball, AngleToDirection(neutralAngles[0, 0], heading), neutralAngles[0, 1]);
                    NewAngleDraw(ball, AngleToDirection(neutralAngles[2, 0], heading), neutralAngles[2, 1]);
                }
                FreezeHit(ball, isPausedHitstun);
            }
            else if (InBallSpecialStun(ball))
            {
                if (hitstun == HitstunState.PONGWALL_STUN && !(player.playerData.specialAmount >= 1 && ball.ballData.wallGrindSide != Side.UP))
                {
                    Vector2 head = new Vector2(ball.bouncingData.pongTurnSide, 0);
                    if (ball.ballData.wallGrindSide == Side.UP)
                    {
                        NewAngleDraw(ball, AngleToDirection(90, Vector2.zero), 1);
                        if (player.playerData.specialAmount < 1)
                        {
                            IBGCBLLKIHA from2 = ball.GetPosition();
                            from2.CGJJEHPPOAN = ConvertTo.Divide(ball.hitbox.bounds.IACOKHPMNGN.GCPKPHMKLBN, HHBCPNCDNDH.NKKIFJJEPOL(2.0m));
                            NewAngleDraw(ball, from2, AngleToDirection(352, heading), 3);
                        }
                    }
                    else if (!isShowAim)
                    {
                        NewAngleDraw(ball, AngleToDirection(270, Vector2.right), 2);
                        NewAngleDraw(ball, AngleToDirection(188, Vector2.right), 4);
                        NewAngleDraw(ball, AngleToDirection(352, Vector2.right), 4);
                    }
                    float num = ConvertTo.Float(ball.bouncingData.flyDirection.GCPKPHMKLBN);
                    if (num == ConvertTo.Float(AngleToDirection(188, Vector2.right).GCPKPHMKLBN) || num == ConvertTo.Float(AngleToDirection(352, Vector2.right).GCPKPHMKLBN) || Mathf.Abs(ConvertTo.Float(ball.bouncingData.flyDirection.CGJJEHPPOAN)) == 1 || num == ConvertTo.Float(AngleToDirection(45, head).GCPKPHMKLBN))
                    {
                        NewAngleDraw(ball, ball.ballData.flyDirection, 4);
                    }
                }
                else if (hitstun.ToString().Contains("SOUNDWAVE"))
                {
                    int speed = 32;
                    int time = 10;

                    Vector2 f = ConvertTo.Vector2(ball.GetPosition());
                    if (hitstun == HitstunState.SOUNDWAVE_STUN)
                    {
                        for (int i = -135; i <= 180; i += 45)
                        {
                            if (i == -90 || i == 90 || i == -270 || i == 270) continue;
                            DrawDistanceLine(f, ConvertTo.AngleToDirection(i, Vector2.right), time, speed, ConvertTo.Colour(i));
                        }
                    }
                    else if (hitstun == HitstunState.SOUNDWAVE_CEILING_STUN)
                    {
                        for (int i = 0; i <= 180; i += 45)
                        {
                            if (i == -90 || i == 90 || i == -270 || i == 270) continue;
                            DrawDistanceLine(f, ConvertTo.AngleToDirection(i, Vector2.right), time, speed, ConvertTo.Colour(i));
                        }
                    }
                    else if (hitstun == HitstunState.SOUNDWAVE_FLOOR_STUN)
                    {
                        for (int i = -135; i <= 0; i += 45)
                        {
                            if (i == -90 || i == 90 || i == -270 || i == 270) continue;
                            DrawDistanceLine(f, ConvertTo.AngleToDirection(i, Vector2.right), time, speed, ConvertTo.Colour(i));
                        }
                        DrawDistanceLine(f, ConvertTo.AngleToDirection(180, Vector3.right), time, speed, ConvertTo.Colour(180));
                    }
                }
                else if (hitstun == HitstunState.GRAFFITI_STUN)
                {
                    NewAngleDraw(ball, ball.ballData.flyDirection, 2);
                }
                else if (hitstun == HitstunState.SHADOWBALL_STUN)
                {
                    BagPlayer bag = (BagPlayer)player;
                    Vector2 shadowHead = ConvertTo.Float(ball.bouncingData.flyDirection.GCPKPHMKLBN) < 0 ? Vector2.left : Vector2.right;
                    NewAngleDraw(ball, AngleToDirection(player.OnGround() ? ConvertTo.Int(bag.hitAngleDownShadow) : player.hitAngleNeutralDownAir, shadowHead), neutralAngles[1, 1]);
                    NewAngleDraw(ball, AngleToDirection(0, shadowHead), neutralAngles[0, 1]);
                    NewAngleDraw(ball, AngleToDirection(ConvertTo.Int(bag.hitAngleUpShadow), shadowHead), neutralAngles[2, 1]);
                }
                if (isPauseBall)
                {
                    FreezeBall(ball);
                }
            }
            else
            {
                hitCancel = false;
                ballCancel = false;
                hitCounter = 0;
                ballCounter = 0;
            }

            if (ball.bouncingData.ballState == BallState.CANDYBALL || ball.hitableData.hitstunState == HitstunState.CANDY_STUN)
            {
                NewAngleDraw(ball, ball.bouncingData.flyDirection, 4);
            }
            else if (hitstun == HitstunState.EATEN_STUN)
            {
                float num = ConvertTo.Float(ball.bouncingData.flyDirection.GCPKPHMKLBN);
                NewAngleDraw(ball, AngleToDirection(neutralAngles[0, 0], heading), neutralAngles[0, 1]);
                if (num == ConvertTo.Float(AngleToDirection(18, heading).GCPKPHMKLBN))
                {
                    NewAngleDraw(ball, ball.ballData.flyDirection, neutralAngles[1, 1]);
                }
                else
                {
                    NewAngleDraw(ball, AngleToDirection(neutralAngles[1, 0], heading), neutralAngles[1, 1]);
                }
                NewAngleDraw(ball, AngleToDirection(neutralAngles[2, 0], heading), neutralAngles[2, 1]);
            }

            if (traceReflectAmount > 0)
            {
                TraceBallOnFollowThrough(ball);
            }
        }

        IBGCBLLKIHA AngleToDirection(int angle, Vector2 heading)
        {
            IBGCBLLKIHA dir = Math.AngleToDirection(new HHBCPNCDNDH(angle));
            dir.GCPKPHMKLBN = ConvertTo.Multiply(dir.GCPKPHMKLBN, ConvertTo.Floatf(heading.x));
            return dir;
        }
        public bool InHitstunByAPlayer(BallEntity ball)
        {
            HitableData ballHitData = ball.hitableData;
            HitstunState hitstunState = ballHitData.hitstunState;
            if (ballHitData.lastHitterIndex == World.NO_PLAYER_INDEX && ballHitData.hitstunState == HitstunState.NONE)
            {
                return false;
            }
            switch (hitstunState)
            {
                case HitstunState.SERVE_STUN:
                case HitstunState.SWING_STUN:
                case HitstunState.CANDY_STUN:
                case HitstunState.CUFFED_STUN:
                case HitstunState.BUBBLE_STUN:
                case HitstunState.WET_STUN:
                case HitstunState.KICKFLIP_STUN:
                case HitstunState.SNIPE_STUN:
                case HitstunState.SOUNDWAVE_START_STUN:
                case HitstunState.TELEPORT_STUN:
                case HitstunState.DOUBLESTRIKE_STUN:
                case HitstunState.HIT_BY_SOUNDBLAST_STUN:
                case HitstunState.EATEN_STUN:
                    return true;
                default:
                    return false;
            }
        }
        public bool InBallSpecialStun(BallEntity ball)
        {
            HitableData ballHitData = ball.hitableData;
            HitstunState hitstunState = ballHitData.hitstunState;
            if (ballHitData.lastHitterIndex == World.NO_PLAYER_INDEX && ballHitData.hitstunState == HitstunState.NONE)
            {
                return false;
            }
            switch (hitstunState)
            {
                case HitstunState.SOUNDWAVE_FLOOR_STUN:
                case HitstunState.SOUNDWAVE_CEILING_STUN:
                case HitstunState.SOUNDWAVE_STUN:
                case HitstunState.GRAFFITI_STUN:
                case HitstunState.SHADOWBALL_STUN:
                    return true;
                case HitstunState.PONGWALL_STUN:
                    return !(ball.GetLastPlayerHitter().playerData.specialAmount >= 1 && ball.ballData.wallGrindSide != Side.UP);
                default:
                    return false;
            }
        }
        void TraceBallOnFollowThrough(BallEntity ball)
        {
            if (preWall != ball.bouncingData.lastWall && ConvertTo.Float(ball.ballData.hitstunTimer) <= 0.3f)
            {
                if (!trace)
                {
                    traceReflect--;
                    trace = true;
                }
                preWall = ball.bouncingData.lastWall;
            }
            else
            {
                trace = false;
            }

            bool isBubble = ball.ballData.ballState == BallState.BUBBLEBALL_OUT || ball.ballData.ballState == BallState.BUBBLEBALL;
            bool inBuubleStun = ball.ballData.hitstunState == HitstunState.BUBBLE_OUT_WALL_STUN || ball.ballData.hitstunState == HitstunState.BUBBLE_WALL_STUN || ball.ballData.hitstunState == HitstunState.BUBBLE_POP_STUN;
            bool soudwaveBall = ball.GetLastPlayerHitter().playerData.specialAmount == 0 && (ball.ballData.hitstunState == HitstunState.SOUNDWAVE_STUN || ball.ballData.ballState == BallState.SOUNDBALL);

            if ((ball.hitableData.hitstunState == HitstunState.NONE && (ball.ballData.ballState == BallState.ACTIVE || isBubble || soudwaveBall)) || ball.InWallTypeStun() || inBuubleStun)
            {
                NewAngleDraw(ball, ball.bouncingData.flyDirection, traceReflect);
            }
            if (ball.InHitstunByAPlayer())
            {
                traceReflect = traceReflectAmount;
            }
        }
        void NewAngleDraw(BallEntity ball, IBGCBLLKIHA angle, int reflections)
        {
            IBGCBLLKIHA pos = ball.GetPosition();
            NewAngleDraw(ball, pos, angle, reflections);
        }
        void NewAngleDraw(BallEntity ball, IBGCBLLKIHA from, IBGCBLLKIHA angle, int reflections)
        {
            World w = World.instance;
            Vector2 pos = ConvertTo.Vector2(from);
            Vector2 dir = ConvertTo.Vector2(angle);
            Vector2 side = Vector2.zero;
            bool warpedHorizontal = false;
            bool warpedVertical = false;
            bool candyBall = ball.ballData.hitstunState == HitstunState.CANDY_STUN || ball.ballData.ballState == BallState.CANDYBALL;
            if (ball.ballData.ballState == BallState.CANDYBALL)
            {
                if (ball.bouncingData.specialHitHoriWall == true)
                {
                    warpedHorizontal = true;
                    side = Vector2.left;
                }
                if (ball.bouncingData.specialHitVertWall == true)
                {
                    warpedVertical = true;
                    side = Vector2.down;
                }
            }
            Color color = ConvertTo.Colour(ConvertTo.Float(ConvertTo.DirectionToAngle(ConvertTo.Vector2f(dir))));
            Color candyColor = Color.white;

            if (isShowAim && ConvertTo.Vector2(ball.bouncingData.flyDirection) != dir)
            {
                color.a = 0.2f;
            }

            for (int i = 0; i < reflections; i++)
            {
                if (candyBall && Mathf.Abs(side.x) == 1)
                {
                    warpedHorizontal = true;
                }
                if (candyBall && Mathf.Abs(side.y) == 1)
                {
                    warpedVertical = true;
                }

                Vector2 wBounds = new Vector2(dir.x < 0 ? ConvertTo.Float(w.stageMin.GCPKPHMKLBN) + 0.1f : ConvertTo.Float(w.stageMax.GCPKPHMKLBN) - 0.1f, dir.y < 0 ? ConvertTo.Float(w.stageMin.CGJJEHPPOAN) + 0.1f : ConvertTo.Float(w.stageMax.CGJJEHPPOAN) - 0.1f);
                Vector2 warpBounds = new Vector2(dir.x > 0 ? ConvertTo.Float(w.stageMin.GCPKPHMKLBN) + 0.1f : ConvertTo.Float(w.stageMax.GCPKPHMKLBN) - 0.1f, dir.y > 0 ? ConvertTo.Float(w.stageMin.CGJJEHPPOAN) + 0.1f : ConvertTo.Float(w.stageMax.CGJJEHPPOAN) - 0.1f);
                double x = pos.x;
                double y = pos.y;
                if (dir.x != 0)
                {
                    // Point-Slope Equation
                    double m = dir.y / dir.x;
                    x = (wBounds.y - pos.y + (m * pos.x)) / m;
                    y = m * wBounds.x - m * pos.x + pos.y;
                }

                Vector2 hitSide;
                if (y >= ConvertTo.Float(w.stageMax.CGJJEHPPOAN) || y <= ConvertTo.Float(w.stageMin.CGJJEHPPOAN) || dir.x == 0)
                {
                    hitSide = Vector2.down;
                }
                else
                {
                    hitSide = Vector2.left;
                }

                Vector2 to = (Mathf.Abs(hitSide.y) == 1) ? new Vector2((float)x, wBounds.y) : new Vector2(wBounds.x, (float)y);
                DrawLine(pos, to, candyBall ? candyColor : color, 10);
                pos = to;

                if (Mathf.Abs(hitSide.x) == 1 && warpedHorizontal || Mathf.Abs(hitSide.y) == 1 && warpedVertical)
                {
                    candyBall = false;
                }

                if (Mathf.Abs(hitSide.y) == 1)
                {
                    if (candyBall)
                    {
                        pos.y = warpBounds.y;
                    }
                    else
                    {
                        dir = Vector2.Reflect(dir, Vector2.down);
                        pos.y = wBounds.y;
                    }
                }
                else
                {
                    if (candyBall)
                    {
                        pos.x = warpBounds.x;
                    }
                    else
                    {
                        dir = Vector2.Reflect(dir, Vector2.left);
                        pos.x = wBounds.x;
                    }
                }
                side = hitSide;
            }
        }
        void FreezeBall(BallEntity ball)
        {
            PlayerEntity hitter = ball.GetLastPlayerHitter();
            if (hitter.playerIndex != 0)
            {
                return;
            }
            if (!ballCancel)
            {
                ballCounter += Time.deltaTime;
                ball.SetHitstunTimer(new HHBCPNCDNDH(1));
            }

            if (ballCounter > 0.25f && InputHandler.GetInput(hitter.player, InputAction.JUMP))
            {
                ballCancel = true;
                ball.SetHitstunTimer(ConvertTo.FramesDuration60fps(1));
            }
        }
        void FreezeHit(BallEntity ball, bool isEnable)
        {
            PlayerEntity hitter = ball.GetLastPlayerHitter();
            if (!isEnable || hitter.playerIndex != 0)
            {
                return;
            }
            string abilityName = hitter.GetCurrentAbilityState().name ?? "";
            float minCancelTime = 0.48f;

            if (!hitCancel)
            {
                if (abilityName.Contains("TELEPORT_START") || abilityName.Contains("TELEPORT_GO") || abilityName.Contains("KID_DOUBLESTRIKE_WIND_UP") || abilityName.Contains("BOSS_SNIPE_WIND_UP"))
                {
                    hitCounter = 0;
                }
                if (abilityName.Contains("STAY") && HHBCPNCDNDH.OAHDEOGKOIM(hitter.GetCurrentAbility().data.abilityStateTimer, ConvertTo.FramesDuration60fps(11)))
                {
                    hitter.GetCurrentAbility().data.abilityStateTimer = ConvertTo.FramesDuration60fps(12);
                }
                hitCounter += Time.deltaTime;
                hitter.SetHitstunTimer(new HHBCPNCDNDH(1));
                ball.SetHitstunTimer(new HHBCPNCDNDH(1));
            }

            if (hitCounter > minCancelTime && InputHandler.GetInput(hitter.player, InputAction.JUMP))
            {
                hitCancel = true;
                hitter.SetHitstunTimer(ConvertTo.FramesDuration60fps(1));
                ball.SetHitstunTimer(ConvertTo.FramesDuration60fps(1));
            }
        }
        void DrawReflect(Vector3 from, Vector3 dir, int reflections, Color c)
        {
            if (reflections == 0)
            {
                return;
            }
            lineMaterial.SetPass(0);
            World w = World.instance;
            BallEntity ball = BallHandler.instance.GetBall(0);
            HHBCPNCDNDH halfBall = HHBCPNCDNDH.FCGOICMIBEA(ball.hitbox.bounds.IACOKHPMNGN.GCPKPHMKLBN, HHBCPNCDNDH.NKKIFJJEPOL(2.2m));
            Vector3 old = from;
            Vector3 boundsmin = new Vector3(ConvertTo.Float(HHBCPNCDNDH.GAFCIOAEGKD(w.stageMin.GCPKPHMKLBN, halfBall)), ConvertTo.Float(HHBCPNCDNDH.GAFCIOAEGKD(w.stageMin.CGJJEHPPOAN, halfBall)));
            Vector3 boundsmax = new Vector3(ConvertTo.Float(HHBCPNCDNDH.FCKBPDNEAOG(w.stageMax.GCPKPHMKLBN, halfBall)), ConvertTo.Float(HHBCPNCDNDH.FCKBPDNEAOG(w.stageMax.CGJJEHPPOAN, halfBall)));

            bool reflect = false;
            while (reflect == false)
            {
                if ((from.x > boundsmin.x) && (from.x < boundsmax.x) && (from.y > boundsmin.y) && (from.y < boundsmax.y))
                {
                    from += dir * 0.01f;
                }
                else
                {
                    Vector3 normal = new Vector3();
                    if (from.x < boundsmin.x)
                    {
                        normal = Vector3.left;
                    }
                    else if (from.x > boundsmax.x)
                    {
                        normal = Vector3.right;
                    }
                    else if (from.y < boundsmin.y)
                    {
                        normal = Vector3.up;
                    }
                    else if (from.y > boundsmax.y)
                    {
                        normal = Vector3.down;
                    }

                    reflect = true;
                    from -= dir * 0.01f;
                    dir = Vector3.Reflect(dir, normal);
                }
            }
            DrawLine(old, from, c, 10);
            DrawReflect(from, dir, reflections - 1, c);
        }
        /// <summary>
        /// Draws line reflections with the colour based on the angle
        /// </summary>
        /// <param name="from">Location to start the first line</param>
        /// <param name="angle"></param>
        /// <param name="vector"></param>
        /// <param name="reflections"></param>
        void DrawReflect(Vector3 from, float angle, Vector3 vector, int reflections)
        {
            Vector3 dir = ConvertTo.AngleToDirection(angle, vector);  // converts the angle to a vector
            Color c = ConvertTo.Colour(angle); // Set's the colour bsaed on the angle being processed


            if (reflections == 0)
            {
                return;
            }
            lineMaterial.SetPass(0);
            World w = World.instance;
            BallEntity ball = BallHandler.instance.GetBall(0);
            HHBCPNCDNDH halfBall = HHBCPNCDNDH.FCGOICMIBEA(ball.hitbox.bounds.IACOKHPMNGN.GCPKPHMKLBN, HHBCPNCDNDH.NKKIFJJEPOL(2.2m));
            Vector3 old = from;
            Vector3 boundsmin = new Vector3(ConvertTo.Float(HHBCPNCDNDH.GAFCIOAEGKD(w.stageMin.GCPKPHMKLBN, halfBall)), ConvertTo.Float(HHBCPNCDNDH.GAFCIOAEGKD(w.stageMin.CGJJEHPPOAN, halfBall)));
            Vector3 boundsmax = new Vector3(ConvertTo.Float(HHBCPNCDNDH.FCKBPDNEAOG(w.stageMax.GCPKPHMKLBN, halfBall)), ConvertTo.Float(HHBCPNCDNDH.FCKBPDNEAOG(w.stageMax.CGJJEHPPOAN, halfBall)));

            bool reflect = false;
            while (reflect == false)
            {
                if ((from.x > boundsmin.x) && (from.x < boundsmax.x) && (from.y > boundsmin.y) && (from.y < boundsmax.y))
                {
                    from += dir * 0.01f;
                }
                else
                {
                    Vector3 normal = new Vector3();
                    if (from.x < boundsmin.x)
                    {
                        normal = Vector3.left;
                    }
                    else if (from.x > boundsmax.x)
                    {
                        normal = Vector3.right;
                    }
                    else if (from.y < boundsmin.y)
                    {
                        normal = Vector3.up;
                    }
                    else if (from.y > boundsmax.y)
                    {
                        normal = Vector3.down;
                    }

                    reflect = true;
                    from -= dir * 0.01f;
                    dir = Vector3.Reflect(dir, normal);
                }
            }
            DrawLine(old, from, c, 10);
            DrawReflect(from, dir, reflections - 1, c);
        }
        /// <summary>
        /// Draws a line from a single point with a duration and speed determining how far to draw the line
        /// </summary>
        /// <param name="from"></param>
        /// <param name="dir"></param>
        /// <param name="durationF"></param>
        /// <param name="speedF"></param>
        /// <param name="color"></param>
        void DrawDistanceLine(Vector3 from, Vector3 dir, int durationF, int speedF, Color color)
        {
            BallEntity ball = BallHandler.instance.GetBall(0);
            float distance = (speedF * durationF) * 0.01f;
            HHBCPNCDNDH halfBall = ConvertTo.Divide(ball.hitbox.bounds.IACOKHPMNGN.GCPKPHMKLBN, ConvertTo.Floatf(2.2f));
            Vector3 boundsmin = ConvertTo.Vector2(ConvertTo.Add(World.instance.stageMin.GCPKPHMKLBN, halfBall), ConvertTo.Add(World.instance.stageMin.CGJJEHPPOAN, halfBall));
            Vector3 boundsmax = ConvertTo.Vector2(ConvertTo.Subtract(World.instance.stageMax.GCPKPHMKLBN, halfBall), ConvertTo.Subtract(World.instance.stageMax.CGJJEHPPOAN, halfBall));

            Vector3 to = from;
            to += dir * distance;

            if (isShowAim && ConvertTo.Vector2(ball.bouncingData.flyDirection) != (Vector2)dir)
            {
                color.a = 0.2f;
            }

            // Prevents the line being drawn out of bounds
            while ((to.x < boundsmin.x) || (to.x > boundsmax.x) || (to.y < boundsmin.y) || (to.y > boundsmax.y))
            {
                to -= dir * 0.01f;
            }
            DrawLine(from, to, color, 10);
        }
        /// <summary>
        /// Draws a line from A to B, with a colour and line thickness
        /// </summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <param name="color"></param>
        /// <param name="thickness">How wide the line is in pixels</param>
        void DrawLine(Vector2 start, Vector2 end, Color color, int thickness)
        {
            float width = thickness * 0.01f; // converts thickness to represent pixels
            BallEntity ball = BallHandler.instance.GetBall(0);
            lineMaterial.SetPass(0);
            Camera camera = GameCamera.gameplayCam;
            GL.PushMatrix();
            GL.Begin(GL.QUADS);
            GL.LoadProjectionMatrix(camera.projectionMatrix);

            float dx = start.x - end.x;
            float dy = start.y - end.y;

            Vector2 rightSide = new Vector2(dy, -dx);
            if (rightSide.magnitude > 0)
            {
                rightSide.Normalize();
                rightSide.Set(rightSide.x * width / 2, rightSide.y * width / 2);
            }
            Vector2 leftSide = new Vector2(-dy, dx);
            if (leftSide.magnitude > 0)
            {
                leftSide.Normalize();
                leftSide.Set(leftSide.x * width / 2, leftSide.y * width / 2);
            }

            Vector2 one = leftSide + start;
            Vector2 two = rightSide + start;
            Vector2 three = rightSide + end;
            Vector2 four = leftSide + end;
            Color color2 = color * 0.25f;
            color2.a = color.a;
            GL.Color(color2);
            GL.Vertex3(one.x, one.y, 0);
            GL.Vertex3(two.x, two.y, 0);
            GL.Color(color);
            GL.Vertex3(three.x, three.y, 0);
            GL.Vertex3(four.x, four.y, 0);
            GL.End();
            GL.PopMatrix();

            color *= 0.45f;
            color.a = color2.a;
            Draw.Cube(new IBGCBLLKIHA(HHBCPNCDNDH.NKKIFJJEPOL((decimal)start.x), HHBCPNCDNDH.NKKIFJJEPOL((decimal)start.y)), ball.hitbox.bounds.IACOKHPMNGN, color, 10f);
            Draw.Cube(new IBGCBLLKIHA(HHBCPNCDNDH.NKKIFJJEPOL((decimal)end.x), HHBCPNCDNDH.NKKIFJJEPOL((decimal)end.y)), ball.hitbox.bounds.IACOKHPMNGN, color, 10f);
        }

        private bool isPausedHitstun;
        private bool isShowAim;
        private bool isPauseBall;
        private bool angleOptions;

        void OnGUI()
        {
            ConvertTo.ResolutionScale();

            _angleDrawerRect = ConvertTo.ClampWindowScaled(_angleDrawerRect);
            _angleDrawerRect = GUI.Window(10, _angleDrawerRect, AngleDrawerWindow, "", ATStyle.BlueWindowStyle);
            if (AdvancedTraining.IsOnline) { Application.Quit(); }
        }

        Vector2 angleOptionsScrollView;
        Rect _angleDrawerRect = new Rect(10, AdvancedTraining.DESIGN_HEIGHT - 210, 240, 210);
        void AngleDrawerWindow(int wId)
        {
            _angleDrawerRect.height = 210;
            GUILayout.Label("Angle Drawer", ATStyle.HeaderStyle);
            GUILayout.BeginVertical(ATStyle.HeaderAreaStyle);
            isPausedHitstun = GUILayout.Toggle(isPausedHitstun, $"Pause Hitstun: <b>{TextHandler.Get(isPausedHitstun ? "TAG_ON" : "TAG_OFF")}</b>", ATStyle.ButtonThin);
            isPauseBall = GUILayout.Toggle(isPauseBall, $"Pause Special Balls: <b>{TextHandler.Get(isPauseBall ? "TAG_ON" : "TAG_OFF")}</b>", ATStyle.ButtonThin);
            isShowAim = GUILayout.Toggle(isShowAim, $"Show Aim Choice: <b>{TextHandler.Get(isShowAim ? "TAG_ON" : "TAG_OFF")}</b>", ATStyle.ButtonThin);
            GUILayout.Label($"Trace ball follow through : " + string.Format((traceReflectAmount > 0) ? $"<b>{traceReflectAmount}</b>" : $"<b>{TextHandler.Get("TAG_OFF")}</b>"), ATStyle.LabStyleLeft);
            traceReflectAmount = (int)Mathf.Round(GUILayout.HorizontalSlider(traceReflectAmount, 0, 10, ATStyle.SliderBackgroundStyle, ATStyle.SliderThumbStyle));
            if (angleOptions = GUILayout.Toggle(angleOptions, "Options", ATStyle.ButtonThinExtend))
            {
                _angleDrawerRect.height = 400;
                angleOptionsScrollView = GUILayout.BeginScrollView(angleOptionsScrollView);
                GUILayout.Label($"Up Reflect Amount : <b>{upReflections}</b>", ATStyle.LabStyleLeft);
                upReflections = (int)Mathf.Round(GUILayout.HorizontalSlider(upReflections, 0, 10, ATStyle.SliderBackgroundStyle, ATStyle.SliderThumbStyle));
                GUILayout.Label($"Down Reflect Amount : <b>{downReflections}</b>", ATStyle.LabStyleLeft);
                downReflections = (int)Mathf.Round(GUILayout.HorizontalSlider(downReflections, 0, 10, ATStyle.SliderBackgroundStyle, ATStyle.SliderThumbStyle));
                GUILayout.Label($"Smash Reflect Amount : <b>{smashReflections}</b>", ATStyle.LabStyleLeft);
                smashReflections = (int)Mathf.Round(GUILayout.HorizontalSlider(smashReflections, 0, 10, ATStyle.SliderBackgroundStyle, ATStyle.SliderThumbStyle));
                GUILayout.Label($"Spike-F Reflect Amount : <b>{spikefReflections}</b>", ATStyle.LabStyleLeft);
                spikefReflections = (int)Mathf.Round(GUILayout.HorizontalSlider(spikefReflections, 0, 10, ATStyle.SliderBackgroundStyle, ATStyle.SliderThumbStyle));
                GUILayout.Label($"Spike-B Reflect Amount : <b>{spikebReflections}</b>", ATStyle.LabStyleLeft);
                spikebReflections = (int)Mathf.Round(GUILayout.HorizontalSlider(spikebReflections, 0, 10, ATStyle.SliderBackgroundStyle, ATStyle.SliderThumbStyle));
                GUILayout.EndScrollView();
            }
            GUILayout.FlexibleSpace();
            GUILayout.EndVertical();
            GUI.DragWindow();
        }

#if DEBUG
        Vector3 PongFrom;
        void PongAngle(BallEntity ball)
        {
            bool hasHitWall = false;
            World w = World.instance;
            HHBCPNCDNDH halfBall = HHBCPNCDNDH.FCGOICMIBEA(ball.hitbox.bounds.IACOKHPMNGN.GCPKPHMKLBN, HHBCPNCDNDH.NKKIFJJEPOL(2.2m));
            Vector3 boundsmin = new Vector3(ConvertTo.Float(HHBCPNCDNDH.GAFCIOAEGKD(w.stageMin.GCPKPHMKLBN, halfBall)), ConvertTo.Float(HHBCPNCDNDH.GAFCIOAEGKD(w.stageMin.CGJJEHPPOAN, halfBall)));
            Vector3 boundsmax = new Vector3(ConvertTo.Float(HHBCPNCDNDH.FCKBPDNEAOG(w.stageMax.GCPKPHMKLBN, halfBall)), ConvertTo.Float(HHBCPNCDNDH.FCKBPDNEAOG(w.stageMax.CGJJEHPPOAN, halfBall)));

            while (!hasHitWall)
            {
                if ((PongFrom.x > boundsmin.x) && (PongFrom.x < boundsmax.x) && (PongFrom.y > boundsmin.y) && (PongFrom.y < boundsmax.y))
                {

                    HHBCPNCDNDH gcpkphmklbn = Math.DirectionToAngle(ball.bouncingData.flyDirection, HHBCPNCDNDH.DBOMOJGKIFI);
                    if (ball.bouncingData.pongTurnSide > 0)
                    {
                        BouncingData bouncingData2 = ball.bouncingData;
                        bouncingData2.turnSpeed = HHBCPNCDNDH.FCKBPDNEAOG(bouncingData2.turnSpeed, HHBCPNCDNDH.AJOCFFLIIIH(HHBCPNCDNDH.NKKIFJJEPOL(6.0m), World.FDELTA_TIME));
                        if (HHBCPNCDNDH.CJBFNLGJNIH(ball.bouncingData.turnSpeed, HHBCPNCDNDH.DBOMOJGKIFI))
                        {
                            ball.bouncingData.turnSpeed = HHBCPNCDNDH.DBOMOJGKIFI;
                        }
                    }
                    else
                    {
                        BouncingData bouncingData3 = ball.bouncingData;
                        bouncingData3.turnSpeed = HHBCPNCDNDH.GAFCIOAEGKD(bouncingData3.turnSpeed, HHBCPNCDNDH.AJOCFFLIIIH(HHBCPNCDNDH.NKKIFJJEPOL(6.0m), World.FDELTA_TIME));
                        if (HHBCPNCDNDH.OCDKNPDIPOB(ball.bouncingData.turnSpeed, HHBCPNCDNDH.DBOMOJGKIFI))
                        {
                            ball.bouncingData.turnSpeed = HHBCPNCDNDH.DBOMOJGKIFI;
                        }
                    }
                    gcpkphmklbn = HHBCPNCDNDH.GAFCIOAEGKD(gcpkphmklbn, HHBCPNCDNDH.AJOCFFLIIIH(HHBCPNCDNDH.AJOCFFLIIIH(ball.bouncingData.turnSpeed, HHBCPNCDNDH.NKKIFJJEPOL(60.0m)), World.FDELTA_TIME));
                    ball.bouncingData.flyDirection = Math.AngleToDirection(HHBCPNCDNDH.GANELPBAOPN(gcpkphmklbn));
                    ball.bouncingData.velocity = IBGCBLLKIHA.AJOCFFLIIIH(ball.bouncingData.flyDirection, new HHBCPNCDNDH(34));
                }
                else
                {
                    hasHitWall = true;
                }
            }
        }
#endif

        public enum Alignment : byte
        {
            INSIDE,
            OUTSIDE,
        }
    }

}
