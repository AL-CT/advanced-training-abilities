﻿using GameplayEntities;
using LLHandlers;
using System.Collections.Generic;
using UnityEngine;
using LLBML.Graphic;

namespace AdvancedTraining
{
    class HitboxMod : MonoBehaviour
    {

        static Material lineMaterial;
        void Start()
        {
            lineMaterial = new Material(Shader.Find("Hidden/Internal-Colored"))
            {
                hideFlags = HideFlags.HideAndDontSave
            };
            lineMaterial.SetInt("_SrcBlend", 5);
            lineMaterial.SetInt("_DstBlend", 10);
            lineMaterial.SetInt("_Cull", 0);
            lineMaterial.SetInt("_ZWrite", 0);
            lineMaterial.SetInt("_ZTest", (int)UnityEngine.Rendering.CompareFunction.Always);
            //CreateStageMesh(World.instance.GetStageCenter(), World.instance.stageSize);

            AudioHandler.PlaySfx(Sfx.MENU_CONFIRM);
            if (AdvancedTraining.IsOnline) { Application.Quit(); }
        }
        void OnEnable()
        {
            AdvancedTraining.Log.LogDebug("[LLBMM] HitBox enabled");
        }

        void OnDisable()
        {
            AdvancedTraining.Log.LogDebug("[LLBMM] HitBox disabled");
        }

        void OnDestroy()
        {
#if !DEBUG
            Destroy(GameObject.Find("LeftMesh"));
            Destroy(GameObject.Find("RightMesh"));
            Destroy(GameObject.Find("TopMesh"));
            Destroy(GameObject.Find("BottomMesh"));
#endif

#if false
            foreach (var obj in blazeHitBoxes)
            {
                Destroy(obj);
            }
#endif
            AudioHandler.PlaySfx(Sfx.MENU_SET);
        }

#if false //CreateBlazeHitboxes - Different way of creating a circle.
        GameObject[] blazeHitBoxes = new GameObject[4];
        void CreateBlazeHitboxes()
        {
            for (int i = 0; i < 4; i++)
            {
                blazeHitBoxes[i] = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                blazeHitBoxes[i].transform.position = new Vector3(0, 0, 0);
                blazeHitBoxes[i].GetComponent<MeshRenderer>().material = lineMaterial;
                blazeHitBoxes[i].GetComponent<MeshRenderer>().material.color = new Color(0, 1, 1, 0.5f);
                blazeHitBoxes[i].transform.localScale = new Vector3(4, 4);
                blazeHitBoxes[i].SetActive(false);
            }
        }
#endif
        static void CreateStageMesh(IBGCBLLKIHA obCenter, IBGCBLLKIHA obSize)
        {

            Vector3[] mainVerts = new Vector3[4];
            Vector3[] leftVerts = new Vector3[4];
            Vector3[] topVerts = new Vector3[4];
            Vector3[] rightVerts = new Vector3[4];
            Vector3[] bottomVerts = new Vector3[4];

            float thicc = 0.1f;
#if GreyStage
            thicc = 3f;
#endif
            Vector2 center = ConvertTo.Vector2(obCenter);
            Vector2 halfSize = ConvertTo.Vector2(obSize) * new Vector2(0.5f, 0.5f);

            mainVerts[0] = new Vector3(center.x - halfSize.x, center.y - halfSize.y);// Bottom Left
            mainVerts[1] = new Vector3(center.x - halfSize.x, center.y + halfSize.y);// Top Left
            mainVerts[2] = new Vector3(center.x + halfSize.x, center.y - halfSize.y);// Bottom Right
            mainVerts[3] = new Vector3(center.x + halfSize.x, center.y + halfSize.y);// Top Right

            leftVerts[0] = mainVerts[0];
            leftVerts[1] = leftVerts[0] - new Vector3(thicc, 0);
            leftVerts[2] = mainVerts[1];
            leftVerts[3] = leftVerts[2] - new Vector3(thicc, 0);

            rightVerts[1] = mainVerts[2];
            rightVerts[0] = rightVerts[1] + new Vector3(thicc, 0);
            rightVerts[3] = mainVerts[3];
            rightVerts[2] = rightVerts[3] + new Vector3(thicc, 0);

            topVerts[0] = leftVerts[3];
            topVerts[1] = topVerts[0] + new Vector3(0, thicc);
            topVerts[2] = rightVerts[2];
            topVerts[3] = topVerts[2] + new Vector3(0, thicc);

            bottomVerts[1] = leftVerts[1];
            bottomVerts[0] = bottomVerts[1] - new Vector3(0, thicc);
            bottomVerts[3] = rightVerts[0];
            bottomVerts[2] = bottomVerts[3] - new Vector3(0, thicc);
            Color color = Color.yellow;
#if GreyStage

            /*camera = Instantiate(GameCamera.GetBackCam());
            camera.name = "ATCamera";
            camera.transform.SetPositionAndRotation(new Vector3(0, 2, -11), Quaternion.identity);
            camera.gameObject.SetActive(false);*/

            string txt2 = "";
            foreach (var name in FindObjectsOfType<GameObject>())
            {
                string str = (name.transform.parent != null) ? name.transform.parent.gameObject.name : "NO PARENT";
                if (str == "Background" || str == "Eclipse")
                {
                    txt2 += $"{name.transform.parent.gameObject.name}/{name.name}\n";
                }
            }
            Debug.Log(txt2);

            GameObject background = GameObject.Find("Background");
            GameObject eclipse = GameObject.Find("Eclipse");
            GameObject bgLayer2 = GameObject.Find("BGLayer2_Animated");
            GameObject pedestrians = GameObject.Find("PedestriansLayer");
            GameObject blimp1 = GameObject.Find("Blimp_Animated 1");
            GameObject blimp2 = GameObject.Find("Blimp_Animated 2");
            GameObject blimp3 = GameObject.Find("Blimp_Animated 3");

            ClearStage(background);
            ClearStage(eclipse);
            ClearStage(bgLayer2);
            ClearStage(pedestrians);
            ClearStage(blimp1);
            ClearStage(blimp2);
            ClearStage(blimp3);
            CreateMeshObject("MainMesh", mainVerts, new Color(0.184f, 0.192f, 0.212f));
            color = new Color(0.125f, 0.133f, 0.145f);
#endif
            CreateMeshObject("LeftMesh", leftVerts, color);
            CreateMeshObject("RightMesh", rightVerts, color);
            CreateMeshObject("TopMesh", topVerts, color);
            CreateMeshObject("BottomMesh", bottomVerts, color);
        }

#if DEBUG

        static void ClearStage(GameObject gameObject)
        {
            if (gameObject != null)
            {
                for (int i = 0; i < gameObject.transform.childCount; i++)
                {
                    bool flag = gameObject.transform.GetChild(i).name.Contains("Positions");
                    bool flag2 = gameObject.transform.GetChild(i).name.Contains("Light");
                    if (flag == false && flag2 == false)
                    {
                        gameObject.transform.GetChild(i).gameObject.SetActive(false);
                    }
                }
                //background.transform.Find("Merged_Environments")?.gameObject.SetActive(false);
                //background.transform.Find("Skybox")?.gameObject.SetActive(false);
                //background.SetActive(false);
                //Debug.Log($"AT-GameObject: {background.transform.parent.gameObject.name}");
            }
        } 
#endif

        static void CreateMeshObject(string meshName, Vector3[] vertices, Color color)
        {
            Mesh mesh = new Mesh();
            int[] triangles = new int[] { 0, 1, 2, 2, 1, 3 };

            mesh.vertices = vertices;
            mesh.triangles = triangles;
            mesh.RecalculateNormals();

            GameObject gameObject = new GameObject(meshName, typeof(MeshFilter), typeof(MeshRenderer));
            //gameObject.transform.parent = camera.transform;
            gameObject.transform.localScale = Vector3.one;

            gameObject.GetComponent<MeshFilter>().mesh = mesh;
            gameObject.GetComponent<MeshRenderer>().material = lineMaterial;
            gameObject.GetComponent<MeshRenderer>().material.color = color;
        }
        void OnPostRender()
        {
            if (AdvancedTraining.IsOnline && AdvancedTraining.CurrentGameMode != GameMode.TRAINING)
            {
                AdvancedTraining.Log.LogError("[LLBMM] \"HitBoxes\" detected online Terminationg LLB");
                AudioHandler.PlaySfx(Sfx.KILL);
                Application.Quit();
                return;
            }
            //DrawCube(World.instance.GetStageCenter(), World.instance.stageSize, Color.white, 10f, Alignment.OUTSIDE);

            //Player.ForAllInMatch
            ALDOKEMAOMB.ICOCPAFKCCE((PlayerEntity player) =>
            {
                if (player.moveBox != null && player.hurtboxes != null && player.hitboxes != null)
                {
                    //DrawCube(player.moveBox.bounds.LOLBPNFNKMI, player.moveBox.bounds.IACOKHPMNGN, Color.white);
                    DrawHurtbox(player);
                    DrawHitbox(player);
                    DrawParry(player);
                    DrawCounterParry(player);
                    DrawBlaze(player);
                    DrawToxicGraffiti(player);
                    ParryInRange(player);
#if false
                        blazeHitBoxes[player.playerIndex].SetActive(player.playerData.inGetUpBlaze);
#endif
                }
            });

            BallHitbox();

            foreach (JEPKNLONCHD boundsf in World.instance.stageBlockList)
            {
                Draw.Cube(boundsf.LOLBPNFNKMI, boundsf.IACOKHPMNGN, Color.grey);
            }
            Draw.Cube(World.instance.volleybalNetBox.LOLBPNFNKMI, World.instance.volleybalNetBox.IACOKHPMNGN, Color.magenta);
            //Draw Hitbox for all corpses
            foreach (ItemEntity itemEntity in ItemHandler.instance.items)
            {
                if (itemEntity.itemData.active)
                {
                    Draw.Cube(itemEntity.hurtbox.bounds.LOLBPNFNKMI, itemEntity.hurtbox.bounds.IACOKHPMNGN, Color.blue);
                }
            }
        }

        internal static void DrawHurtbox(PlayerEntity playerEntity)
        {
            // Draw player Hurtboxes
            foreach (KeyValuePair<string, PlayerBox> keyValuePair in playerEntity.hurtboxes)
            {
                PlayerBox value = keyValuePair.Value;
                if (value != null && value.active)
                {
                    //Boolean to show if the player when they can jump while charging
                    //bool flag = player.abilityData.abilityState.Contains("CHARGE") && ConvertTo.GreaterThanAndEqual(player.abilityData.abilityStateTimer, ConvertTo.Multiply(player.chargeMaxDuration, 0.5f));
                    Color color = (playerEntity.IsRecovering() || !playerEntity.hitableData.canBeHitByBall) ? Color.white : Color.blue;
                    Draw.Cube(value.bounds.LOLBPNFNKMI, value.bounds.IACOKHPMNGN, color);
                }
            }
        }

        internal static void DrawHitbox(PlayerEntity playerEntity)
        {
            // Draw player Hitboxes
            foreach (KeyValuePair<string, PlayerHitbox> keyValuePair2 in playerEntity.hitboxes)
            {
                PlayerBox value2 = keyValuePair2.Value;
                //Full charge Indicator
                if (ConvertTo.GreaterThanAndEqual(playerEntity.abilityData.abilityStateTimer, ConvertTo.Subtract(playerEntity.chargeMaxDuration, ConvertTo.Add(playerEntity.fullChargeMargin, ConvertTo.FramesDuration60fps(1)))) && playerEntity.abilityData.abilityState.Contains("CHARGE") && keyValuePair2.Key.Contains("NEUTRAL"))
                {
                    Draw.Cube(value2.bounds.LOLBPNFNKMI, value2.bounds.IACOKHPMNGN, Color.white);
                }
                // Draw Active Player Hitbox
                if (value2 != null && (value2.active))
                {
                    Draw.Cube(value2.bounds.LOLBPNFNKMI, value2.bounds.IACOKHPMNGN, Color.red);
                }
            }
        }

        private static void DrawParry(PlayerEntity playerEntity)
        {
            if (playerEntity.parryBox == null || !HHBCPNCDNDH.OAHDEOGKOIM(playerEntity.playerData.parryActiveTimer, HHBCPNCDNDH.NKKIFJJEPOL(0)))
                return;
            Draw.Cube(playerEntity.parryBox.bounds.LOLBPNFNKMI, playerEntity.parryBox.bounds.IACOKHPMNGN, Color.yellow);
        }


        static void DrawBlaze(PlayerEntity playerEntity)
        {
            //Draws the Blaze hitbox
            if (playerEntity.playerData.inGetUpBlaze)
            {
                /*IBGCBLLKIHA position = player.GetPosition();
                position.CGJJEHPPOAN = ConvertTo.Add(World.instance.stageMin.CGJJEHPPOAN, ConvertTo.Multiply(HHBCPNCDNDH.NKKIFJJEPOL(128), World.FPIXEL_SIZE));
                blazeHitBoxes[player.playerIndex].transform.position = ConvertTo.Vector2(position);*/
                IBGCBLLKIHA center = playerEntity.GetPosition();
                center.CGJJEHPPOAN = ConvertTo.Add(World.instance.stageMin.CGJJEHPPOAN, ConvertTo.Multiply(HHBCPNCDNDH.NKKIFJJEPOL(128), World.FPIXEL_SIZE));
                Draw.Polygon(center, 2, Color.cyan);
            }
        }
        static void DrawCounterParry(PlayerEntity playerEntity)
        {
            //Draws the Grab/Counter parry hitbox
            if (playerEntity.counterParryBox != null && playerEntity.playerData.counterParryActive)
            {
                Draw.Cube(playerEntity.counterParryBox.bounds.LOLBPNFNKMI, playerEntity.counterParryBox.bounds.IACOKHPMNGN, Color.green);
            }
        }

        static void DrawToxicGraffiti(PlayerEntity playerEntity)
        {
            if (playerEntity.character == Character.GRAF)
            {
                GrafPlayer graf = (GrafPlayer)playerEntity;
                // Draws Toxic's Graffit hitbox
                if (graf != null && graf.graffitiAbility.data.specialAmount != 0 && graf.graffitiAbility.data.specialBool == true)
                {
                    Draw.Cube(graf.graffitiBox.bounds.LOLBPNFNKMI, graf.graffitiBox.bounds.IACOKHPMNGN, Color.green);
                }
            }
        }

        static void ParryInRange(PlayerEntity playerEntity)
        {
            // Show Player 2's Parrybox while Player 1 is in range of it
            ALDOKEMAOMB.ICOCPAFKCCE((PlayerEntity otherPlayer) =>
            {
                if (playerEntity.playerIndex != otherPlayer.playerIndex)
                {
                    string flag1 = ALDOKEMAOMB.BJDPHEHJJJK(otherPlayer.playerIndex).JCCIAMJEODH.CheckHitboxes(playerEntity.parryBox, false, true);
                    if (flag1 != PlayerBox.NONE && !ALDOKEMAOMB.BJDPHEHJJJK(otherPlayer.playerIndex).JCCIAMJEODH.hitboxes[flag1].cantBeParried && BallHandler.instance.GetBall().InHitstunByPlayer(playerEntity.playerIndex))
                    {
                        Draw.Cube(playerEntity.parryBox.bounds.LOLBPNFNKMI, playerEntity.parryBox.bounds.IACOKHPMNGN, Color.white);
                    }
                }
            });
        }

        static void BallHitbox()
        {
            // Draws each balls' hitbox
            BallEntity[] balls = BallHandler.instance.balls;
            foreach (BallEntity ball in balls)
            {
                Color color = Color.blue;
                PlayerEntity playerEntity = ball.GetLastPlayerHitter() ?? ALDOKEMAOMB.BJDPHEHJJJK(0).JCCIAMJEODH;
                //Highlights when you can parry with a yellow ball hitbox
                /*if (HHBCPNCDNDH.OAHDEOGKOIM(playerEntity.RemainingHitstunTime(), playerEntity.startParryBeforeEndMinusThis) && playerEntity.attackingData.energy != 0 && ball.InHitstunByAPlayer())
                {
                    color = Color.yellow;
                }*/

                if (ball.InHitstunByAPlayer())
                {
                    //Highlights when you can special the ball with a cyan ball hitbox
                    bool flag = HHBCPNCDNDH.HPLPMEAOJPM(playerEntity.RemainingHitstunTime(), HHBCPNCDNDH.NKKIFJJEPOL(1.75m));
                    bool flag2 = HHBCPNCDNDH.OAHDEOGKOIM(playerEntity.RemainingHitstunTime(), ConvertTo.FramesDuration60fps(8));
                    bool flag3 = HHBCPNCDNDH.HPLPMEAOJPM(playerEntity.hitableData.hitstunDuration, ConvertTo.FramesDuration60fps(9));
                    bool dairOn = playerEntity.GetCurrentAbility() != playerEntity.downAirSwingAbility;
                    Color lightBlue = new Color(0f, 0.8f, 1f);
                    if (playerEntity.HasFullEnergy())
                    {
                        if (playerEntity.character == Character.PONG)
                        {
                            if (flag && flag2) color = lightBlue;
                        }
                        else if (playerEntity.character == Character.BAG)
                        {
                            if (flag && (flag2 || flag3)) color = lightBlue;
                        }
                        else if (playerEntity.character == Character.KID)
                        {
                            if (flag && dairOn) color = lightBlue;
                        }
                        else if (playerEntity.character == Character.ROBOT)
                        {
                            if (dairOn) color = lightBlue;
                        }
                        else
                        {
                            color = lightBlue;
                        }
                    }

                    if (ball.hitableData.hitstunState == HitstunState.WET_STUN && playerEntity.character == Character.SKATE && playerEntity.playerData.specialAmount <= 1)
                    {
                        color = lightBlue;
                    }
                }

                Draw.Cube(ball.hitbox.bounds.LOLBPNFNKMI, ball.hitbox.bounds.IACOKHPMNGN, color);
            }
        }
    }
}

